package com.together.togetherv2kt

import android.app.Application
import com.orhanobut.logger.Logger
import com.orhanobut.logger.AndroidLogAdapter

class PangApp : Application() {

    companion object {
        var sMemberSeq = 0
    }

    override fun onCreate() {
        super.onCreate()
        Logger.addLogAdapter(AndroidLogAdapter())
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }

}