package com.together.togetherv2kt.common

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.android.material.snackbar.Snackbar
import java.util.*
import com.together.togetherv2kt.R
import com.together.togetherv2kt.ui.frag.PangAddFragment


open class BaseFragment : Fragment() {

    val mDialog by lazy {
        MaterialDialog(context!!)
            .customView(R.layout.dialog_item_progress_bar)
            .show {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
    }
    val TIME_FORMAT by lazy { setLocaleTimeFormat() }
    val DATE_FORMAT by lazy { setLocaleDateFormat() }
    val DATE_TIME_FORMAT by lazy { setLocaleDateTimeFormat() }
    lateinit var bContext: Context

    val yearDelimiter by lazy { getString(R.string.year) }
    val monthDelimiter by lazy { getString(R.string.month) }
    val dayOfMonthDelimiter by lazy { getString(R.string.day_of_month) }
    val hourDelimiter by lazy { getString(R.string.hour) }
    val minuteDelimiter by lazy { getString(R.string.minute) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        bContext = context
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PangAddFragment().apply {
            }
    }

    fun showSnackBarShort(_view: View, _msg: String) {
        Snackbar.make(_view, _msg, Snackbar.LENGTH_SHORT).show()
    }

    open fun showSnackBarLong(_view: View, _msg: String) {
        Snackbar.make(_view, _msg, Snackbar.LENGTH_LONG).show()
    }

    fun hideKeyboard(_context: Context, _view: View) {
        val inputMethodManager = _context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(_view.windowToken, 0)
    }

    private fun setLocaleTimeFormat(): String {
        val locale = Locale.getDefault()
        val language = locale.language
        return if (language == "en") {
            "HH${getString(R.string.time_hour_seperator)}mm"
        } else {
            "HH${getString(R.string.time_hour_seperator)}mm${getString(R.string.time_minute_seperator)}"
        }
    }

    private fun setLocaleDateFormat(): String {
        val locale = Locale.getDefault()
        val language = locale.language
        return if (language == "en") {
            "yyyy${getString(R.string.date_year_seperator)}" + "MM${getString(R.string.date_month_seperator)}" + "dd"
        } else {
            "yyyy${getString(R.string.date_year_seperator)}" +
                    "MM${getString(R.string.date_month_seperator)}" +
                    "dd${getString(R.string.date_day_seperator)}"
        }
    }

    private fun setLocaleDateTimeFormat(): String {
        val locale = Locale.getDefault()
        val language = locale.language
        return if (language == "en") {
            "yyyy${getString(R.string.date_year_seperator)}" +
                    "MM${getString(R.string.date_month_seperator)}" +
                    "dd" +
                    " " +
                    "HH${getString(R.string.time_hour_seperator)}mm"
        } else {
            "yyyy${getString(R.string.date_year_seperator)}" +
                    "MM${getString(R.string.date_month_seperator)}" +
                    "dd${getString(R.string.date_day_seperator)}" +
                    "  " +
                    "HH${getString(R.string.time_hour_seperator)}" +
                    "mm${getString(R.string.time_minute_seperator)}"
        }
    }
}


/*

    private var listener: OnCheckingGpsOnLsn? = null
// TODO: Rename method, update argument and hook method into UI event
fun onButtonPressed(uri: Uri) {
    listener?.isGpsOn(uri)
}

override fun onAttach(context: Context) {
    super.onAttach(context)
    if (context is OnCheckingGpsOnLsn) {
        listener = context
    } else {
        throw RuntimeException(context.toString() + " must implement OnCheckingGpsOnLsn")
    }
}

override fun onDetach() {
    super.onDetach()
    listener = null
}

*/
/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 *
 *
 * See the Android Training lesson [Communicating with Other Fragments]
 * (http://developer.android.com/training/basics/fragments/communicating.html)
 * for more information.
 *//*

interface OnCheckingGpsOnLsn {
    // TODO: Update argument type and name
    fun isGpsOn(uri: Uri)
}
*/
