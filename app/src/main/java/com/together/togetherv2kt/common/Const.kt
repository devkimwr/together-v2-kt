package com.together.togetherv2kt.common

object Const {

    /**
     * VALUE
     */
    const val DEFAULT_ZOOM = 15
    const val MAP_ZOOM_IN_MIN = 13
    const val MAP_ZOOM_IN_MID = 15
    const val MAP_ZOOM_IN_MAX = 20
    const val AUTO_LOGIN_TRUE = 90
    const val AUTO_LOGIN_FALSE = 10
    const val LAT_TOKYO_STATION = 35.68123620000001
    const val LNG_TOKYO_STATION = 139.76712480000003
    const val LAT_SHIBUYA = 35.6619707
    const val LNG_SHIBUYA = 139.703795
    const val LAT_SILICON_VALLEY = 37.387474
    const val LNG_SILICON_VALLEY = 139.703795
    const val KO_LOCALE_CODE = "ko"
    const val JP_LOCALE_CODE = "ja"
    const val EN_LOCALE_CODE = "en"

    /**
     * REQUEST CODE
     */
    const val GPS_REQUEST = 1000
    const val REQUEST_CHECK_SETTINGS = 1

    /**
     * PREFS
     */
    const val FN_MEMBER_GUID = "FN_MEMBER_GUID"
    const val FN_MEMBER_SEQ = "FN_MEMBER_SEQ"
    const val FN_AUTO_LOGIN = "FN_AUTO_LOGIN"
    const val KN_MEMBER_GUID = "KN_MEMBER_GUID"
    const val KN_MEMBER_SEQ = "KN_MEMBER_SEQ"
    const val KN_IS_AUTO_LOGIN = "KN_IS_AUTO_LOGIN"

    /**
     * DIALOG
     */
    const val MODE_BIRTH_DAY_SHOW = 0
    const val MODE_BIRTH_DAY_SENDING = 1

    /**
     * FORMAT
     */
    const val SQL_DATETIME_FORMAT = "yyyy-MM-dd HH:mm"
    const val SQL_DATETIME_PARSE_2_MINUTE = "yyyyMMddHHmm"
    const val DATETIME_PARSE_2_SECONDS = "yyyyMMddHHmmss"
    const val DATA_FORMAT_2_SHOW = "yyyy-MM-dd"
}