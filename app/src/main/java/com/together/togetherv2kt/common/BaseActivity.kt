package com.together.togetherv2kt.common

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.android.material.snackbar.Snackbar
import com.together.togetherv2kt.R

open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSnackBarShort(_view: View, _msg: String) {
        Snackbar.make(_view, _msg, Snackbar.LENGTH_SHORT).show()
    }

    fun showSnackBarLong(_view: View, _msg: String) {
        Snackbar.make(_view, _msg, Snackbar.LENGTH_LONG).show()
    }

    fun showShortToast(_context: Context, _msg: String) {
        Toast.makeText(_context, _msg, Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(_context: Context, _msg: String) {
        Toast.makeText(_context, _msg, Toast.LENGTH_LONG).show()
    }

    val mDialog by lazy {
        MaterialDialog(this)
            .customView(R.layout.dialog_item_progress_bar)
            .show {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
    }
}