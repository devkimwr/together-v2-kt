package com.together.togetherv2kt

import android.net.*
import android.os.Bundle
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.lib.NetLib
import com.together.togetherv2kt.lib.PrefsLib
import com.together.togetherv2kt.common.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AppGateActivity : BaseActivity() {

    private lateinit var mConnectManager: ConnectivityManager
    private val mGuid by lazy { PrefsLib.getMemberGuid(this) }

    private val netCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            Logger.d(network)
            checkAutoLogin()
        }

        override fun onLost(network: Network?) {
            Logger.d(network)
            showLongToast(this@AppGateActivity, "Lost Network!!")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_gate)
        mConnectManager = NetLib.checkNetState(this, netCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        mConnectManager.unregisterNetworkCallback(netCallback)
    }

    // TODO what is the best to get Auto login value from local or online?
    private fun checkAutoLogin() {
        when (PrefsLib.getAutoLoginVal(this)) {
            Const.AUTO_LOGIN_TRUE -> callGetMemberSeq(mGuid)
            else -> GoLib.goAccount(this)
        }
    }

    /**
     * API
     */
    //TODO response type 이 Int 인데 성공?
    private fun callGetMemberSeq(guidMember: String) {
        mDialog.show()
        val call: Call<Int>? = ApiClient.providePangApi().callGetMemberSeq(guidMember)
        call?.enqueue(object : Callback<Int> {
            override fun onResponse(call: Call<Int>, response: Response<Int>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        mDialog.dismiss()
                        PangApp.sMemberSeq = it
                        GoLib.goMain(this@AppGateActivity)
                    }
                }
            }

            override fun onFailure(call: Call<Int>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }
}
