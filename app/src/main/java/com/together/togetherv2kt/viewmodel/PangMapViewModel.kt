package com.together.togetherv2kt.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng

class PangMapViewModel : ViewModel() {
    val isGpsOn = MutableLiveData<Boolean>()
    val latLng = MutableLiveData<LatLng>()

    fun observeGpsOn(isOn: Boolean) {
        isGpsOn.postValue(isOn)
    }
}