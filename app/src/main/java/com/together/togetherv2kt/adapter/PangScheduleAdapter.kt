package com.together.togetherv2kt.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.data.remote.model.response.ResSchedules
import com.together.togetherv2kt.ui.frag.list.PangScheduleListFragmentDirections
import kotlinx.android.synthetic.main.list_item_pang_schedule.view.*

/**
 * Pang list adapter
 */
class PangScheduleAdapter :
    ListAdapter<ResSchedules, PangScheduleAdapter.ViewHolder>(PangScheduleDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_pang_schedule,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { pangs ->
            with(holder.itemView) {
                tvContentPangScheduleDetail.text = pangs.note
                tvPlacePangScheduleDetail.text = pangs.placeMeetUp
                tvDatePangScheduleDetail.text = DateLib.formatDateTimeShow(Const.DATA_FORMAT_2_SHOW, pangs.dateMeetUp)
                setOnClickListener(createOnClickListener(pangs))
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    private fun createOnClickListener(_pScheduleInfo: ResSchedules): View.OnClickListener {
        return View.OnClickListener {
            val direction =
                PangScheduleListFragmentDirections.actionPangScheduleListFragToPangScheduleDetailFrag(_pScheduleInfo)
            it.findNavController().navigate(direction)
        }
    }
}

private class PangScheduleDiffCallback : DiffUtil.ItemCallback<ResSchedules>() {
    override fun areItemsTheSame(oldItem: ResSchedules, newItem: ResSchedules): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ResSchedules, newItem: ResSchedules): Boolean {
        return oldItem == newItem
    }
}