package com.together.togetherv2kt.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.data.remote.model.response.ResPangs
import com.together.togetherv2kt.ui.frag.list.PangMainListFragmentDirections
import kotlinx.android.synthetic.main.list_item_pang.view.*

/**
 * Pang list adapter
 */
class PangListAdapter :
    ListAdapter<ResPangs, PangListAdapter.ViewHolder>(PangDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_pang,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { pangs ->
            with(holder.itemView) {
                pangContent.text = pangs.note
                station.text = pangs.placeMeetUp
                date.text = DateLib.formatDateTimeShow(Const.DATA_FORMAT_2_SHOW, pangs.dateMeetUp)
                setOnClickListener(createOnClickListener(pangs))
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    private fun createOnClickListener(pangDetailInfo: ResPangs): View.OnClickListener {
        return View.OnClickListener {
            val direction =
                PangMainListFragmentDirections.actionPangListToPangDetail(pangDetailInfo)
            it.findNavController().navigate(direction)
        }
    }
}

private class PangDiffCallback : DiffUtil.ItemCallback<ResPangs>() {
    override fun areItemsTheSame(oldItem: ResPangs, newItem: ResPangs): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ResPangs, newItem: ResPangs): Boolean {
        return oldItem == newItem
    }
}