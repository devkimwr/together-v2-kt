package com.together.togetherv2kt.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.together.togetherv2kt.R
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.data.remote.model.response.ResPangsEdit
import kotlinx.android.synthetic.main.list_item_pang_edit.view.*

/**
 * Pang Edit list
 */
class PangEditListAdapter :
    ListAdapter<ResPangsEdit, PangEditListAdapter.ViewHolder>(PangEditDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_pang_edit, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { pangs ->
            with(holder.itemView) {
                contentMeetUpEditList.text = pangs.note
                placeMeetUpEditList.text = pangs.placeMeetUp
              // dateMeetUpEditList.text = pangs.dateMeetUp
                setOnClickListener(createOnClickListener(pangs))
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    private fun createOnClickListener(_pangEditItem: ResPangsEdit): View.OnClickListener {
        return View.OnClickListener {
            GoLib.goPangEdit(it, _pangEditItem)
        }
    }
}

private class PangEditDiffCallback : DiffUtil.ItemCallback<ResPangsEdit>() {
    override fun areItemsTheSame(oldItem: ResPangsEdit, newItem: ResPangsEdit): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ResPangsEdit, newItem: ResPangsEdit): Boolean {
        return oldItem == newItem
    }
}