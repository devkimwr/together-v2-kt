package com.together.togetherv2kt.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.data.remote.model.response.ResPangsFav
import kotlinx.android.synthetic.main.list_item_pang_fav.view.*

/**
 * Pang list adapter
 */
class PangFavListAdapter :
    ListAdapter<ResPangsFav, PangFavListAdapter.ViewHolder>(PangFavDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_pang_fav, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { pangs ->
            with(holder.itemView) {
                tvNoteFavList.text = pangs.note
                tvPlaceFavList.text = pangs.placeMeetUp
                tvDateFavList.text = DateLib.formatDateTimeShow(Const.DATA_FORMAT_2_SHOW, pangs.dateMeetUp)
                setOnClickListener(createOnClickListener(pangs))
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    private fun createOnClickListener(_pFavInfo: ResPangsFav): View.OnClickListener {
        return View.OnClickListener { GoLib.goPangFav(it, _pFavInfo) }
    }
}

private class PangFavDiffCallback : DiffUtil.ItemCallback<ResPangsFav>() {
    override fun areItemsTheSame(oldItem: ResPangsFav, newItem: ResPangsFav): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ResPangsFav, newItem: ResPangsFav): Boolean {
        return oldItem == newItem
    }
}