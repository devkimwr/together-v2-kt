package com.together.togetherv2kt.ui.frag.list


import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.R
import com.together.togetherv2kt.adapter.PangListAdapter
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.data.remote.model.response.ResPangs
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_pang_main_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


private const val EMPTY_PANG_LIST = 10
private const val CHECKED_ASC = 20
private const val CHECKED_DESC = 30

/**
 * パンリスト画面
 *
 *
 *TODO
 *  Chat listに変換
 *  機能
 *  ・参加したパン情報
 *  ・chat項目を持っていることは参加情報をわかること。
 *  　
 *
 *
 */
class PangMainListFragment : BaseFragment() {

    lateinit var mThisView: View
    private var mPangOrderStatus = CHECKED_ASC

    private lateinit var mMenuDesc: MenuItem
    private lateinit var mMenuAsc: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.invalidateOptionsMenu()
        mThisView = inflater.inflate(R.layout.fragment_pang_main_list, container, false)
        return mThisView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callGetPangMainList()
        setBtnGoPangAdd()
        setRefreshLayout()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_list, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        mMenuDesc = menu.findItem(R.id.menu_desc)
        mMenuAsc = menu.findItem(R.id.menu_asc)

        if (mPangOrderStatus == CHECKED_ASC) {
            mMenuAsc.isChecked = true
        } else {
            mMenuDesc.isChecked = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_asc -> {
                mMenuAsc.isChecked = true
                mPangOrderStatus = CHECKED_ASC
                callGetPangMainList()
            }

            R.id.menu_desc -> {
                mMenuDesc.isChecked = true
                mPangOrderStatus = CHECKED_DESC
                callGetPangMainList()
            }

            R.id.menu_to_fav_list -> {
                GoLib.goPangFavList(view!!)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initAdapter(_pangs: List<ResPangs>) {
        val adapter = PangListAdapter()
        rcvPangList.layoutManager = LinearLayoutManager(context)
        rcvPangList.adapter = adapter
        rcvPangList.addOnScrollListener(makeOnScrollLsn())
        adapter.submitList(_pangs)
    }


    private fun makeOnScrollLsn(): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !fbGoPangAdd.isShown)
                    fbGoPangAdd.show()
                else if (dy > 0 && fbGoPangAdd.isShown)
                    fbGoPangAdd.hide()
            }
        }
    }

    private fun setBtnGoPangAdd() {
        fbGoPangAdd.setOnClickListener {
            GoLib.goPangAdd(it)
        }
    }

    private fun setRefreshLayout() {
        refreshLayoutPangMain.setOnRefreshListener {
            callGetPangMainList()
            refreshLayoutPangMain.isRefreshing = false
        }
    }

    /**
     * API
     */
    private fun callGetPangMainList() {
        mDialog.show()
        val mSeq = PangApp.sMemberSeq
        val call: Call<List<ResPangs>>? = ApiClient.providePangApi().callGetPangs(mSeq, mPangOrderStatus)
        call?.enqueue(object : Callback<List<ResPangs>> {
            override fun onResponse(call: Call<List<ResPangs>>, response: Response<List<ResPangs>>) {
                mDialog.hide()
                if (!response.isSuccessful) {
                    setResponseError(response)
                } else {
                    response.body()?.let {
                        initAdapter(it)
                    }
                }
            }

            override fun onFailure(call: Call<List<ResPangs>>, t: Throwable) {
                mDialog.hide()
                Logger.d(t.stackTrace)
            }
        })
    }

    private fun <T> setResponseError(_res: Response<T>) {
        // toString()とstring()は違う
        val errCode = _res.errorBody()?.string()?.toInt()
        when (errCode) {
            EMPTY_PANG_LIST ->
                showSnackBarShort(view!!, "Has not pang list")
        }

        return
    }
}
// TODO
