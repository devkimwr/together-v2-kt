package com.together.togetherv2kt.ui.frag

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.BaseFragment
import com.together.togetherv2kt.lib.GoLib

//TODO 駅ごとの掲示板ページ.
class StationsBoardFragment : BaseFragment() {

    private val mArgs: StationsBoardFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_stations_board, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                GoLib.goPlaceMap(view!!,  mArgs.stationInfo.name)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    /**
     * API
     */

}
