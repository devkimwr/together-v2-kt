package com.together.togetherv2kt.ui.frag.account


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.navArgs
import com.together.togetherv2kt.R
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.lib.TextLib
import com.together.togetherv2kt.lib.ValidLib
import com.together.togetherv2kt.data.remote.model.request.ReqSignUp
import com.together.togetherv2kt.ui.act.AccountActivity
import kotlinx.android.synthetic.main.fragment_sign_up_account_info.*

/**
 * アカウント情報登録画面（パスワード、名前、メールアドレス）
 */
class SignUpAccountInfoFragment : Fragment() {

    private val mArgs: SignUpAccountInfoFragmentArgs by navArgs()
    // 値の有効化情報配列 - 有効：1 , 無効:0
    private val mIsEmptyEdTexts = IntArray(2)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_up_account_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setEmail()
        setEdtName()
        setEdtPw()
        setBtnNextScreen()
    }

    private fun setEmail() {
        edtEmailAccountInfo.setText(mArgs.emailSuccess)
    }

    private fun setEdtName() {
        edtNameSignUp.addTextChangedListener {
            when {
                it.isNullOrEmpty() -> {
                    tilNameAccountInfo.error = getString(R.string.error_empty_name)
                    mIsEmptyEdTexts[0] = 0
                }
                else -> {
                    tilNameAccountInfo.error = null
                    mIsEmptyEdTexts[0] = 1
                }
            }

            btnNextAccountInfo.isEnabled = !ValidLib.isEmptyEdtArr(mIsEmptyEdTexts)
        }
    }

    //TODO pw
    private fun setEdtPw() {
        edtPwSignUp.addTextChangedListener {
            if (!TextLib.isValidPassword(it!!)) {
                tilPwSignUp.error = getString(R.string.error_not_match_pw)
                mIsEmptyEdTexts[1] = 0
            } else {
                tilPwSignUp.error = null
                mIsEmptyEdTexts[1] = 1
            }

            btnNextAccountInfo.isEnabled = !ValidLib.isEmptyEdtArr(mIsEmptyEdTexts)
        }
    }

    private fun setBtnNextScreen() {
        btnNextAccountInfo.setOnClickListener {
            gatherAccountData()
            view?.let {
                GoLib.goSignUpExtraInfo(it)
            }
        }
    }

    private fun gatherAccountData(): ReqSignUp {
        AccountActivity.sReqSignUp.email = mArgs.emailSuccess
        AccountActivity.sReqSignUp.name = TextLib.gatherTextFromEdt(edtNameSignUp)
        AccountActivity.sReqSignUp.pw = TextLib.gatherTextFromEdt(edtPwSignUp)
        return AccountActivity.sReqSignUp
    }
}
