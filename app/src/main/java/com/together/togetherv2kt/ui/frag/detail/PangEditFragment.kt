package com.together.togetherv2kt.ui.frag.detail


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.*
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.navArgs
import com.together.togetherv2kt.R
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.lib.DialogLib
import com.together.togetherv2kt.lib.TextLib
import com.together.togetherv2kt.lib.ValidLib
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_pang_edit.*

/**
 * 作成したパン修正
 */
class PangEditFragment : BaseFragment() {
    private val mArgs: PangEditFragmentArgs by navArgs()
    private lateinit var mMenu: Menu

    private val arr4EdtCheck = IntArray(2)
    private val menuItemSave by lazy {
        mMenu.findItem(R.id.menu_p_edit_save)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pang_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTxtPangEditItem()
        setTvDate()
        setTvTime()
        setPlaceTxtWatcher()
        setNoteTxtWatcher()
        setTimePicker()
        setDatePicker()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_edit, menu)
        mMenu = menu
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val menuItemSave = mMenu.findItem(R.id.menu_p_edit_save)
        val menuItemEdit = mMenu.findItem(R.id.menu_p_edit)

        when (item.itemId) {
            R.id.menu_p_edit -> {
                enabledPangEditWidget(true)
                menuItemSave.isVisible = true
                menuItemEdit.isVisible = false
            }

            R.id.menu_p_edit_save -> {
                if (ValidLib.isEmptyEdtArr(arr4EdtCheck)) {
                    //   menuItemSave.isEnabled = false
                } else {
                    //TODO 수정한 정보 저장하는 함수 실장
                    enabledPangEditWidget(false)
                    menuItemEdit.isVisible = true
                    menuItemSave.isVisible = false
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setTxtPangEditItem() {
        val time = DateLib.getTimeFromDateTimeFormat(TIME_FORMAT, mArgs.pangEditInfo.dateMeetUp)
        val date = DateLib.getDateFromDateTimeFormat(DATE_FORMAT, mArgs.pangEditInfo.dateMeetUp)
        edtNotePangEdit.setText(mArgs.pangEditInfo.note)
        tvDatePangEdit.text = date
        tvTimePangEdit.text = time
        edtPlacePangEdit.setText(mArgs.pangEditInfo.placeMeetUp)
    }

    private fun enabledPangEditWidget(status: Boolean) {
        tvDatePangEdit.isClickable = status
        tvTimePangEdit.isClickable = status
        edtPlacePangEdit.isEnabled = status
        edtNotePangEdit.isEnabled = status
        edtPlacePangEdit.isClickable = status
        edtNotePangEdit.isFocusable = status
        edtPlacePangEdit.isFocusable = status
    }

    private fun setTvDate() {
        tvDatePangEdit.setOnClickListener {
            showSnackBarShort(view!!, "DATE")
        }
    }

    private fun setTvTime() {
        tvTimePangEdit.setOnClickListener {
            showSnackBarShort(view!!, "TIME")
        }
    }

    private fun setPlaceTxtWatcher() {
        edtPlacePangEdit.addTextChangedListener {
            if (it.isNullOrBlank()) {
                menuItemSave.isEnabled = false
                tilPlacePangEdit.error = getString(R.string.error_empty_place)
                arr4EdtCheck[0] = 0
            } else {
                menuItemSave.isEnabled = true
                tilPlacePangEdit.error = null
                arr4EdtCheck[0] = 1

            }
        }
    }

    private fun setNoteTxtWatcher() {
        edtNotePangEdit.addTextChangedListener {
            if (it.isNullOrBlank()) {
                menuItemSave.isEnabled = false
                tilNotePangEdit.error = getString(R.string.error_empty_note)
                arr4EdtCheck[1] = 0
            } else {
                menuItemSave.isEnabled = true
                tilNotePangEdit.error = null
                arr4EdtCheck[1] = 1
            }
        }
    }

    private fun setTimePicker() {
        tvTimePangEdit.setOnClickListener {
            DialogLib.makeTimePicker(it.context, timeListener).show()
        }
    }

    private fun setDatePicker() {
        tvDatePangEdit.setOnClickListener {
            DialogLib.makeDatePicker(it.context, dateListener).show()
        }
    }

    private val dateListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
        val convertedMonth = TextLib.makeTwoDigit(month + 1)
        val convertedDayOfMonth = TextLib.makeTwoDigit(dayOfMonth)
        val resultTxt = "$year$yearDelimiter$convertedMonth$monthDelimiter$convertedDayOfMonth$dayOfMonthDelimiter"
        tvDatePangEdit.text = resultTxt
    }

    private val timeListener = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
        val convertedHour = TextLib.makeTwoDigit(hourOfDay)
        val convertedMinute = TextLib.makeTwoDigit(minute)
        val resultTxtTime = "$convertedHour$hourDelimiter$convertedMinute$minuteDelimiter"
        tvTimePangEdit.text = resultTxtTime
    }

    // TODO パンUpdate API
/*    private fun callUpdatePang() {
        context?.run {
            val memberSeq = PangApp.sMemberSeq

            val call: Call<ResponseBody> = ApiClient.providePangApi(this).callUpdatePang(memberSeq, pangSeq)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.code() == 200) {
                        response.body()?.let {

                        }
                    } else {

                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                   // setErrorChangeFavIcon(FAV_IS_ADDED)
                    Logger.d(t.stackTrace)
                }
            })
        }
    }*/
}
