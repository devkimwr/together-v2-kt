package com.together.togetherv2kt.ui.frag.account


import android.os.Bundle
import android.view.*
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.lib.PrefsLib
import com.together.togetherv2kt.lib.TextLib
import com.together.togetherv2kt.lib.ValidLib
import com.together.togetherv2kt.data.remote.model.request.ReqLogin
import com.together.togetherv2kt.data.remote.model.response.ResLogin
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val ERR_CODE_DID_NOT_REGISTER_EMAIL = 10
private const val ERR_CODE_DID_NOT_REGISTER_PW = 20

class LoginFragment : BaseFragment() {

    //checked : 90 , not checked : 10
    private var mAutoLoginChecked = 10
    // 値の有効化情報配列 - 有効：1 , 無効:0
    private val mEdtEmptyArr = IntArray(2)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_account, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.signUpAccount -> goVerifyEmailFrag()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setChkBoxAutoLogin()
        setEmailTxtWatcher()
        setPwTxtWatcher()
        setBtnDoLogin()
    }

    private fun goVerifyEmailFrag() {
        val direction = LoginFragmentDirections.actionToSignUp()
        findNavController().navigate(direction)
    }

    fun goSignUpExtraInfoFrag() {
        val direction = LoginFragmentDirections.actionToSignUp()
        findNavController().navigate(direction)
    }

    private fun setBtnDoLogin() {
        btnLogin.setOnClickListener {
            val emailTxt = TextLib.gatherTextFromEdt(edtEmailLogin)
            val pwTxt = TextLib.gatherTextFromEdt(edtPwLogin)
            val loginObj =
                ReqLogin(emailTxt, pwTxt, mAutoLoginChecked)
            callLogin(loginObj)
        }
    }

    private fun setChkBoxAutoLogin() {
        checkAutoLogin.setOnCheckedChangeListener { buttonView, isChecked ->
            mAutoLoginChecked = when (isChecked) {
                true -> Const.AUTO_LOGIN_TRUE
                false -> Const.AUTO_LOGIN_FALSE
            }
        }
    }

    private fun setEmailTxtWatcher() {
        edtEmailLogin.addTextChangedListener {
            // 空文字、Emailの形式が正しい場合
            if (!it.isNullOrEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches()) {
                tilEmailLogin.error = null
                mEdtEmptyArr[0] = 1
            }

            if (it.isNullOrEmpty()) {
                tilEmailLogin.error = getString(R.string.error_empty_email)
                mEdtEmptyArr[0] = 0
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches()) {
                tilEmailLogin.error = getString(R.string.error_not_email_format)
                mEdtEmptyArr[0] = 0
            }

            btnLogin.isEnabled = !ValidLib.isEmptyEdtArr(mEdtEmptyArr)
        }
    }

    private fun setPwTxtWatcher() {
        edtPwLogin.addTextChangedListener {
            if (!TextLib.isValidPassword(it!!)) {
                tilPwLogin.error = getString(R.string.error_not_match_pw)
                mEdtEmptyArr[1] = 0
            } else {
                tilPwLogin.error = null
                mEdtEmptyArr[1] = 1
            }

            btnLogin.isEnabled = !ValidLib.isEmptyEdtArr(mEdtEmptyArr)
        }
    }

    private fun setAutoLoginVal() {
        context?.let {
            if (mAutoLoginChecked == Const.AUTO_LOGIN_TRUE) {
                PrefsLib.setAutoLoginVal(it, Const.AUTO_LOGIN_TRUE)
            } else {
                PrefsLib.setAutoLoginVal(it, Const.AUTO_LOGIN_FALSE)
            }
        }
    }

    private fun saveGuidMember(_guid: String) {
        PrefsLib.setMemberGuid(context!!, _guid)
    }

    /**
     * API
     */
    private fun callLogin(_loginObj: ReqLogin) {
        mDialog.show()
        val call: Call<ResLogin> = ApiClient.providePangApi().callLogin(_loginObj)
        call.enqueue(object : Callback<ResLogin> {
            override fun onResponse(call: Call<ResLogin>, response: Response<ResLogin>) {
                mDialog.dismiss()
                if (!response.isSuccessful) {
                    setResponseError(response)
                } else {
                    response.body()?.let { resLogin ->
                        PangApp.sMemberSeq = resLogin.memberSeq
                        setAutoLoginVal()
                        saveGuidMember(resLogin.memberGuid)
                        GoLib.goMain(context!!)
                    }
                }
            }

            override fun onFailure(call: Call<ResLogin>, t: Throwable) {
                Logger.d(t.localizedMessage)
            }
        })

    }

    private fun <T> setResponseError(_res: Response<T>) {
        // toString()とstring()は違う
        val errCode = _res.errorBody()?.string()?.toInt()
        when (errCode) {
            ERR_CODE_DID_NOT_REGISTER_EMAIL ->
                showSnackBarShort(view!!, "Did Not Register Email")
            ERR_CODE_DID_NOT_REGISTER_PW ->
                showSnackBarShort(view!!, "Did Not Register Password")
        }
        return
    }
}
