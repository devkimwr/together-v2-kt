package com.together.togetherv2kt.ui.frag.list


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.R
import com.together.togetherv2kt.adapter.PangFavListAdapter
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.NetLib
import com.together.togetherv2kt.data.remote.model.response.ResPangsFav
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_pang_fav_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PangFavListFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pang_fav_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callGetFavPangList()
    }

    private fun initAdapter(_pangs: List<ResPangsFav>) {
        val adapter = PangFavListAdapter()
        rcvPangFav.layoutManager = LinearLayoutManager(context)
        rcvPangFav.adapter = adapter
        adapter.submitList(_pangs)
    }

    /**
     * API
     */
    // TODO ing get fav list
    private fun callGetFavPangList() {
        mDialog.show()
        val mSeq = PangApp.sMemberSeq
        val call: Call<List<ResPangsFav>> = ApiClient.providePangApi().callGetFavPangList(mSeq)
        call.enqueue(object : Callback<List<ResPangsFav>> {
            override fun onResponse(call: Call<List<ResPangsFav>>, response: Response<List<ResPangsFav>>) {
                mDialog.hide()
                val res = NetLib.getResponseData(response) ?: return
                Logger.d(res)
                initAdapter(res)
            }

            override fun onFailure(call: Call<List<ResPangsFav>>, t: Throwable) {
                mDialog.hide()
                Logger.d(t.stackTrace)
            }
        })
    }
}

//TODO swipe refresh list 실장