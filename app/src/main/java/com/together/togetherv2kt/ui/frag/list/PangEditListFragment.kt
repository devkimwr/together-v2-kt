package com.together.togetherv2kt.ui.frag.list


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.together.togetherv2kt.PangApp

import com.together.togetherv2kt.R
import com.together.togetherv2kt.adapter.PangEditListAdapter
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.data.remote.model.response.ResPangs
import com.together.togetherv2kt.data.remote.model.response.ResPangsEdit
import kotlinx.android.synthetic.main.fragment_pang_edit_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PangEditListFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pang_edit_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callGetPangEditList()
    }

    private fun initAdapter(_pangs: List<ResPangsEdit>) {
        val adapter = PangEditListAdapter()
        rcvPangEditList.layoutManager = LinearLayoutManager(context)
        rcvPangEditList.adapter = adapter
        adapter.submitList(_pangs)
    }


    /**
     * API
     */
    private fun callGetPangEditList(): List<ResPangsEdit> {
        var pangs: List<ResPangsEdit> = listOf()
        val mSeq = PangApp.sMemberSeq
        val call: Call<List<ResPangsEdit>>? = ApiClient.providePangApi().callGetPangsEdit(mSeq)
        call?.enqueue(object : Callback<List<ResPangsEdit>> {
            override fun onResponse(call: Call<List<ResPangsEdit>>, response: Response<List<ResPangsEdit>>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        initAdapter(it)
                        pangs = it
                    }
                }
            }

            override fun onFailure(call: Call<List<ResPangsEdit>>, t: Throwable) {
                t.stackTrace
            }
        })

        return pangs
    }
}
