package com.together.togetherv2kt.ui.frag.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.FOCUS_BLOCK_DESCENDANTS
import android.widget.Button
import android.widget.NumberPicker
import androidx.core.widget.addTextChangedListener
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.lib.PrefsLib
import com.together.togetherv2kt.lib.TextLib
import com.together.togetherv2kt.lib.ValidLib
import com.together.togetherv2kt.data.remote.model.request.ReqSignUp
import com.together.togetherv2kt.ui.act.AccountActivity
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up_extra_info.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SignUpExtraInfoFragment : BaseFragment() {

    private lateinit var mView: View
    // 値の有効化情報配列 - 有効：1 , 無効:0
    private val isExtraDataEmpty = IntArray(2)
    // 10 : Female 90: Male
    private var mGender = 10
    private var mGuid = ""
    private var mBirthday = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_sign_up_extra_info, container, false)
        mView = v
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBtnRegisterMember()
        setGenderChipGroup()
        setTvBirthDay()
        setEdtPlaceMn()
    }

    private fun setBtnRegisterMember() {
        btnRegisterMember.setOnClickListener {
            val reqSignUpObj = gatherSignUpData()
            callSignUp(reqSignUpObj)
        }
    }

    //TODO gender chip
    private fun setGenderChipGroup() {
        chipGroupGender.setOnCheckedChangeListener { chipGroup, i ->
            when (chipGroup.checkedChipId) {
                R.id.chipFeMaleSignUpExtra -> mGender = 10
                R.id.chipMaleSignUpExtra -> mGender = 20
            }
        }
    }

    //TODO birthday edt
    private fun setTvBirthDay() {
        tvBirthDaySignUpExtra.setOnClickListener {
            val birthdayPicker = makeBirthdayPicker()
            birthdayPicker.show()
            isExtraDataEmpty[0] = 1
            btnRegisterMember.isEnabled = !ValidLib.isEmptyEdtArr(isExtraDataEmpty)
        }
    }

    //TODO place main int
    private fun setEdtPlaceMn() {
        edtPlaceMainSignUpExtra.addTextChangedListener {
            when {
                it.isNullOrEmpty() -> {
                    tilPlaceMainSignUpExtra.error = getString(R.string.error_empty_place_mn)
                    isExtraDataEmpty[1] = 0
                }
                else -> {
                    tilPlaceMainSignUpExtra.error = null
                    isExtraDataEmpty[1] = 1
                }
            }

            btnRegisterMember.isEnabled = !ValidLib.isEmptyEdtArr(isExtraDataEmpty)
        }
    }

    private fun makeBirthdayPicker(): MaterialDialog {
        val dialog = MaterialDialog(context!!).customView(R.layout.dialog_birth_day)
        val pickerYear = dialog.findViewById<NumberPicker>(R.id.pickerBirthdayYear)
        val pickerMonth = dialog.findViewById<NumberPicker>(R.id.pickerBirthdayMonth)
        val pickerDay = dialog.findViewById<NumberPicker>(R.id.pickerBirthdayDay)
        val btnYes = dialog.findViewById<Button>(R.id.btnPickerBirthdayYes)
        val btnNo = dialog.findViewById<Button>(R.id.btnPickerBirthdayNo)

        pickerYear.maxValue = 2020
        pickerYear.minValue = 1970
        pickerMonth.maxValue = 12
        pickerMonth.minValue = 1
        pickerDay.maxValue = 31
        pickerDay.minValue = 1
        // keyboard 가 안 올라오게 함(포커스 없앰)
        pickerYear.descendantFocusability = FOCUS_BLOCK_DESCENDANTS
        pickerMonth.descendantFocusability = FOCUS_BLOCK_DESCENDANTS
        pickerDay.descendantFocusability = FOCUS_BLOCK_DESCENDANTS

        btnYes.setOnClickListener {
            mBirthday = joinBirthdayDateTxt(pickerYear, pickerMonth, pickerDay, Const.MODE_BIRTH_DAY_SENDING)
            tvBirthDaySignUpExtra.text =
                joinBirthdayDateTxt(pickerYear, pickerMonth, pickerDay, Const.MODE_BIRTH_DAY_SHOW)

            dialog.dismiss()
        }

        btnNo.setOnClickListener {
            dialog.dismiss()
        }

        return dialog
    }

    private fun joinBirthdayDateTxt(
        _yearPic: NumberPicker,
        _monthPic: NumberPicker,
        _dayPic: NumberPicker,
        _mode: Int
    ): String {
        var birthdayDate = ""
        when (_mode) {
            // 생일 뒤에 년, 월, 일의 문자를 결합 예: 2018/03/22
            Const.MODE_BIRTH_DAY_SHOW -> {
                val year = "${_yearPic.value}${getString(R.string.date_year_seperator)}"
                val month = "${TextLib.makeTwoDigit(_monthPic.value)}${getString(R.string.date_month_seperator)}"
                val day = TextLib.makeTwoDigit(_dayPic.value)
                birthdayDate = "$year$month$day"
            }

            // 생일숫자만 결합 예: 20180322
            Const.MODE_BIRTH_DAY_SENDING -> {
                val year = "${_yearPic.value}"
                val month = TextLib.makeTwoDigit(_monthPic.value)
                val day = TextLib.makeTwoDigit(_dayPic.value)
                birthdayDate = "$year$month$day"

            }
        }

        return birthdayDate
    }

    // 送信する会員登録情報を集める
    private fun gatherSignUpData(): ReqSignUp {
        val guid = UUID.randomUUID().toString()
        AccountActivity.sReqSignUp.guidMember = guid
        mGuid = guid
        AccountActivity.sReqSignUp.placeMn = TextLib.gatherTextFromEdt(edtPlaceMainSignUpExtra)
        AccountActivity.sReqSignUp.placeSub = TextLib.gatherTextFromEdt(edtPlaceSubSignUpExtra)
        AccountActivity.sReqSignUp.birthDay = mBirthday
        AccountActivity.sReqSignUp.note = TextLib.gatherTextFromEdt(edtNoteSignUpExtra)
        AccountActivity.sReqSignUp.gender = mGender
        return AccountActivity.sReqSignUp
    }

    /**
     * API
     */
    //TODO API 화원가입 정보 등록
    private fun callSignUp(_reqSignUp: ReqSignUp) {
        val call: Call<String> = ApiClient.providePangApi().callSignUp(_reqSignUp)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.code() == 200) {
                    GoLib.goMain(context!!)
                    PrefsLib.setMemberGuid(context!!, mGuid)
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }
}
