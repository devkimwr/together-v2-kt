package com.together.togetherv2kt.ui.frag.detail


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.afollestad.materialdialogs.MaterialDialog
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.lib.GoLib
import kotlinx.android.synthetic.main.fragment_pang_schedule.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * パンスケジュール詳細画面
 */
class PangScheduleFragment : Fragment() {
    private val mArgs: PangScheduleFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.invalidateOptionsMenu()
        return inflater.inflate(R.layout.fragment_pang_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPangDetailInfoTxt()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_schedule_detail, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_join_cancel)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_join_cancel -> joinCancelDialog()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun joinCancelDialog() {
        context?.let {
            MaterialDialog(it).show {
                message(text = getString(R.string.dl_msg_join_cancel))
                positiveButton(R.string.dl_positive_btn_y) { dialog ->
                    callCancelJoinPang()

                }
                negativeButton(R.string.dl_negative_btn_n) { dialog ->
                    // Do something
                }
            }
        }
    }

    // TODO 참가 취소 기능? 취소한다면 어느 화면?
    private fun goPangList() {
        /* val direction =
             PangScheduleFragment

         this.findNavController().navigate(direction)*/
    }

    private fun setPangDetailInfoTxt() {
        val dateSchedule = DateLib.formatDateTimeShow(Const.DATA_FORMAT_2_SHOW, mArgs.pScheduleInfo.dateMeetUp)
        edtDatePangSchedule.setText(dateSchedule)
        edtPlacePangSchedule.setText(mArgs.pScheduleInfo.placeMeetUp)
        edtNotePangSchedule.setText(mArgs.pScheduleInfo.note)
    }

    /**
     * API
     */
    private fun callCancelJoinPang() {
        //TODO パンSeq , 会員Seq
        val pSeq = mArgs.pScheduleInfo.seq
        val mSeq = PangApp.sMemberSeq
        val call: Call<ResponseBody> = ApiClient.providePangApi().callCancelJoinPang(pSeq, mSeq)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.isSuccessful.let {
                    GoLib.navigateUp(view!!)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }
}