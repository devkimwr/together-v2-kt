package com.together.togetherv2kt.ui.act


import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.R
import com.together.togetherv2kt.custom.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tb_main.*
import android.app.Activity
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.together.togetherv2kt.common.BaseActivity
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.data.remote.model.response.Station
import com.together.togetherv2kt.ui.frag.PangMapFragment
import com.together.togetherv2kt.viewmodel.PangMapViewModel


const val TAG_PANG_MAP_FRAG = "bottomNavigation#0"

class MainActivity : BaseActivity() {

    private var currentNavController: LiveData<NavController>? = null
    private val mPangMapViewModel by lazy {
        ViewModelProviders.of(this).get(PangMapViewModel::class.java)
    }

    private val mPangMapFrag by lazy {
        getFragment(TAG_PANG_MAP_FRAG) as PangMapFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        } // Else, need to wait for onRestoreInstanceState

        setSupportActionBar(tb)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }

    /**
     * Overriding popBackStack is necessary in this case if the app is started from the deep link.
     */
    override fun onBackPressed() {
        Logger.d("onBackPressed>>")
        if (currentNavController?.value?.popBackStack() != true) {
            //super.onBackPressed()
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.GPS_REQUEST -> {
                mDialog.dismiss()
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        // Called accept gps on
                        mDialog.dismiss()
                        mPangMapFrag.getDeviceLocation()
                        mPangMapViewModel.isGpsOn.postValue(true)
                    }

                    Activity.RESULT_CANCELED -> {
                        // Called not accept gps on todo what process if not accept gps on
                        mPangMapViewModel.isGpsOn.postValue(false)
                        Logger.d("kim", "Activity.RESULT_CANCELED")
                    }
                }
            }
        }
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupBottomNavigationBar() {
        val navGraphIds =
            listOf(
                R.navigation.pang_map,
                R.navigation.pang_list,
                R.navigation.pang_schedule_list,
                R.navigation.nv_my_page
            )
        // Setup the bottom navigation view with a list of navigation graphs
        val controller = bottomNavVw.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.nav_host_container,
            intent = intent
        )

        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, Observer { navController ->
            setupActionBarWithNavController(navController)
            setOnDestinationChangedListener(navController)
        })

        // BottomNavViewの位置選択コード：  bottomNavVw.selectedItemId = R.id.pang_list
        currentNavController = controller
    }

    private fun getFragment(_tag: String): Fragment {
        val existingFragment0 =
            supportFragmentManager.findFragmentByTag(_tag) as NavHostFragment
        return existingFragment0.childFragmentManager.fragments[0]
    }

    /**
     * When changing fragment, Set tool bar title each fragment
     */
    private fun setOnDestinationChangedListener(_navController: NavController) {
        _navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.label) {
                getString(R.string.tb_title_pang_map) -> {
                    tbList.visibility = View.GONE
                }

                getString(R.string.label_station_board) -> {
                    val stationInfo = arguments?.get("stationInfo") as? Station
                    tbTitle.text = stationInfo?.name
                    tbList.visibility = View.VISIBLE
                }

                else -> {
                    tbTitle.text = "${destination.label}"
                    tbList.visibility = View.VISIBLE
                }
            }
        }
    }
}