package com.together.togetherv2kt.ui.act

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.BaseActivity
import com.together.togetherv2kt.data.remote.model.request.ReqSignUp
import kotlinx.android.synthetic.main.tb_main.*

/**
 * アカウント管理
 */
class AccountActivity : BaseActivity() {

    private lateinit var mNavControllerAccount: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        var sReqSignUp = ReqSignUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        setTb()
    }

    private fun setTb() {
        mNavControllerAccount = findNavController(R.id.accountHostFrag)
        appBarConfiguration = AppBarConfiguration(mNavControllerAccount.graph)
        setSupportActionBar(tb)
        setTbTitle(tb)
        setupActionBarWithNavController(mNavControllerAccount, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return mNavControllerAccount.navigateUp()
    }

    // Tool Barのタイトル設定
    private fun setTbTitle(tb: androidx.appcompat.widget.Toolbar) {
        mNavControllerAccount.addOnDestinationChangedListener { controller, destination, arguments ->
            val tbTitleTxt = destination.label
            tbTitle.text = tbTitleTxt
        }
    }
}
