package com.together.togetherv2kt.ui.frag.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.R
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.GoLib.goSignUpAccount
import com.together.togetherv2kt.lib.TextLib
import com.together.togetherv2kt.lib.ValidLib.checkEdtEmailError
import com.together.togetherv2kt.data.remote.model.request.ReqCheckVerifyNum
import com.together.togetherv2kt.data.remote.model.response.ResResultEmailVerify
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up_verify_email.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * 会員登録時に認証メールを送信する画面
 */
class SignUpEmailVerifyFragment : BaseFragment() {

    private var mInputtedEmail = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_sign_up_verify_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBtnSendVerifyNumEmail()
        setBtnSendVerifyNum()
        setEdtSendVerifyNumEmail()
        setEdtSendVerifyNum()
    }

    private fun setBtnSendVerifyNumEmail() {
        btnSendVerifyNumEmail.setOnClickListener {
            hideKeyboard(context!!, it)
            mInputtedEmail = TextLib.gatherTextFromEdt(edtSendVerifyNumEmail)
            callGetVerificationNum(mInputtedEmail)
        }
    }

    // Send VerifyNum
    private fun setBtnSendVerifyNum() {
        btnSendVerifyNum.setOnClickListener {
            val verifyNum = TextLib.gatherTextFromEdt(edtSendVerifyNum)
            val reqCheckVerifyNum =
                ReqCheckVerifyNum(mInputtedEmail, verifyNum)
            callVerifyNum(reqCheckVerifyNum)
        }
    }

    private fun setEdtSendVerifyNumEmail() {
        edtSendVerifyNumEmail.addTextChangedListener {
            when (checkEdtEmailError(it!!)) {
                0 -> tilSendVerifyEmail.error = getString(R.string.error_empty_email)
                1 -> tilSendVerifyEmail.error = getString(R.string.error_not_email_format)
                9 -> {
                    tilSendVerifyEmail.error = null
                    btnSendVerifyNumEmail.isEnabled = true
                }
            }
        }

        edtSendVerifyNumEmail.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    hideKeyboard(context!!, v)
                    true
                }
                else -> false
            }
        }

        edtSendVerifyNumEmail.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                tilSendVerifyEmail.defaultHintTextColor = activity?.getColorStateList(R.color.gray)
            }
        }
    }

    private fun setEdtSendVerifyNum() {
        edtSendVerifyNum?.addTextChangedListener {
            if (it.isNullOrEmpty()) {
                tilSendVerifyNum.error = getString(R.string.error_empty_verify_num)
            } else {
                tilSendVerifyNum.error = null
                btnSendVerifyNum.isEnabled = true
            }
        }

        edtSendVerifyNum.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    hideKeyboard(context!!, v)
                    true
                }
                else -> false
            }
        }
    }

    /**
     * API
     */
    //TODO
    private fun callGetVerificationNum(_email: String) {
        val call: Call<ResponseBody>? = ApiClient.providePangApi().callSendVerifyEmail(_email)
        call?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (!response.isSuccessful) {
                    showSnackBarShort(view!!, "Duplicate Email !!")
                } else {
                    layoutSendVerifyEmail.visibility = View.GONE
                    layoutSendVerifyNum.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                showSnackBarShort(view!!, "Failed Verify Email")
                Logger.d(t.stackTrace)
            }
        })
    }

    //
    private fun callVerifyNum(_reqCheckVerifyNum: ReqCheckVerifyNum) {
        val call: Call<ResResultEmailVerify>? = ApiClient.providePangApi().callCheckVerifyNum(_reqCheckVerifyNum)
        call?.enqueue(object : Callback<ResResultEmailVerify> {
            override fun onResponse(call: Call<ResResultEmailVerify>, response: Response<ResResultEmailVerify>) {
                response.body()?.run {
                    goSignUpAccount(view!!, this.email)
                }
            }

            override fun onFailure(call: Call<ResResultEmailVerify>, t: Throwable) {
                showSnackBarShort(view!!, "Failed Verify Email")
                Logger.d(t.stackTrace)
            }
        })
    }
}
