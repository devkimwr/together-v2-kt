package com.together.togetherv2kt.ui.frag.list


import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp

import com.together.togetherv2kt.R
import com.together.togetherv2kt.adapter.PangScheduleAdapter
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.data.remote.model.response.ResSchedules
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_pang_schedule_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PangScheduleListFragment : BaseFragment() {

    /**
     * Data default
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setHasOptionsMenu(true)
    }

    /**
     * View default
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.invalidateOptionsMenu()
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pang_schedule_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callGetPangMainList()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_schedule_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_schedule_history -> {
                showSnackBarShort(view!!, "history")
            }

        }
        return super.onOptionsItemSelected(item)

    }

    private fun initAdapter(_pSchedule: List<ResSchedules>) {
        val pangAdapter = PangScheduleAdapter()
        rcvPangSchedule.run {
            layoutManager = LinearLayoutManager(context)
            adapter = pangAdapter
        }

        pangAdapter.submitList(_pSchedule)
    }

    /**
     * API
     */
    private fun callGetPangMainList() {
        mDialog.show()
        val mSeq = PangApp.sMemberSeq
        val call: Call<List<ResSchedules>>? = ApiClient.providePangApi().callGetSchedules(mSeq)
        call?.enqueue(object : Callback<List<ResSchedules>> {
            override fun onResponse(call: Call<List<ResSchedules>>, response: Response<List<ResSchedules>>) {
                mDialog.hide()
                if (!response.isSuccessful) {
                    setResponseError(response)
                } else {
                    response.body()?.let {
                        initAdapter(it)
                    }
                }
            }

            override fun onFailure(call: Call<List<ResSchedules>>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }

    private fun <T> setResponseError(_res: Response<T>) {
        MaterialDialog(context!!).show {
            message(null, "Empty Schedule")
        }
    }
}