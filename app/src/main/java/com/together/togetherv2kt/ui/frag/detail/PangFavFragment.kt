package com.together.togetherv2kt.ui.frag.detail


import android.os.Bundle
import android.view.*
import androidx.navigation.fragment.navArgs
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.lib.DialogLib
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_pang_fav.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Pang Favorite
 * */
class PangFavFragment : BaseFragment() {

    private val mArgs: PangFavFragmentArgs by navArgs()
    private lateinit var mView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewFrag = inflater.inflate(R.layout.fragment_pang_fav, container, false)
        mView = viewFrag
        return viewFrag
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTxtFavPangInfo()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_fav, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_favorite_p_fav -> showDialogDelFavPang(goFavPangList)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showDialogDelFavPang(func4Run: () -> Unit) {
        val textDialogArr = IntArray(3)
        textDialogArr[0] = R.string.dl_msg_del_fav_pang
        textDialogArr[1] = R.string.dl_positive_btn_y
        textDialogArr[2] = R.string.dl_negative_btn_n

        context?.let {
            DialogLib.show2BtnDialog(it, textDialogArr, func4Run)
        } ?: Logger.d(context)
    }

    private fun setTxtFavPangInfo() {
        edtNotePangFav.setText(mArgs.pangFavInfo.note)
        edtDatePangFav.setText(DateLib.formatDateTimeShow(Const.DATA_FORMAT_2_SHOW, mArgs.pangFavInfo.dateMeetUp))
        edtPlacePangFav.setText(mArgs.pangFavInfo.placeMeetUp)
    }

    private val goFavPangList: () -> Unit = {
        callDelFavPang()
    }


    /**
     * API
     */
    // パンお気に入り削除
    private fun callDelFavPang() {
        val memberSeq = PangApp.sMemberSeq
        val pangSeq = mArgs.pangFavInfo.seq
        val call: Call<ResponseBody> = ApiClient.providePangApi().callDelFavPang(memberSeq, pangSeq)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                when {
                    response.code() == 200 -> GoLib.navigateUp(mView)
                    else -> showSnackBarShort(mView, getString(R.string.failed_del_fav_pang))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                showSnackBarShort(mView, getString(R.string.failed_del_fav_pang))
                Logger.d(t.stackTrace)
            }
        })
    }
}
