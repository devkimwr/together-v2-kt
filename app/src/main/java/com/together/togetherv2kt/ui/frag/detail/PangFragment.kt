package com.together.togetherv2kt.ui.frag.detail


import android.os.Bundle
import android.view.*
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_pang_main.*
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.R
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.lib.DialogLib
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.data.remote.model.request.ReqFavPang
import com.together.togetherv2kt.data.remote.model.request.ReqPangJoin
import com.together.togetherv2kt.common.BaseFragment
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val FAV_IS_ADDED = 9
const val FAV_IS_DELETED = 0

/**
 * パン詳細
 */
class PangFragment : BaseFragment() {
    // リスト画面で選択した情報を保持
    private val mArgs: PangFragmentArgs by navArgs()
    private lateinit var mMenuItem: MenuItem
    var mFavIconStatus = FAV_IS_DELETED

    //TODO M 参加ボタン押した場合、参加人情報 + パン情報API
    private val goPangs: () -> Unit = {
        callJoinPang()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.invalidateOptionsMenu()
        return inflater.inflate(R.layout.fragment_pang_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPangDetailInfoTxt()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_detail, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        showMenuIconHasEntry(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        mMenuItem = item
        when (item.itemId) {
            R.id.menu_favorite -> changeFavIconStatus()
            R.id.menu_join -> showJoinDialog(goPangs)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun changeFavIconStatus() {
        when (mFavIconStatus) {
            FAV_IS_DELETED -> callAddFavPang()
            FAV_IS_ADDED -> callDelFavPang()
        }
    }

    /**
     * Menu Icon設定
     * 参加者：無
     * 参加希望者：参加Text, お気に入りIcon
     */
    private fun showMenuIconHasEntry(_menu: Menu) {
        val joinMemberId = mArgs.pangDetailInfo.joinMemberId
        // Show like and join menu if not join member
        if (joinMemberId.isNullOrEmpty()) {
            _menu.findItem(R.id.menu_favorite).isVisible = true
            _menu.findItem(R.id.menu_join).isVisible = true
        } else {
            _menu.findItem(R.id.menu_favorite).isVisible = false
            _menu.findItem(R.id.menu_join).isVisible = false
        }
    }

    private fun showJoinDialog(func4Run: () -> Unit) {
        val textDialogArr = IntArray(3)
        textDialogArr[0] = R.string.dl_msg_join
        textDialogArr[1] = R.string.pang_detail_dl_positive_btn
        textDialogArr[2] = R.string.pang_detail_dl_negative_btn

        context?.let {
            DialogLib.show2BtnDialog(it, textDialogArr, func4Run)
        } ?: Logger.d("Context is null in join dialog ")
    }

    private fun setPangDetailInfoTxt() {
        edtDatePangDetail.setText(DateLib.formatDateTimeShow(DATE_TIME_FORMAT, mArgs.pangDetailInfo.dateMeetUp))
        edtPlacePangDetail.setText(mArgs.pangDetailInfo.placeMeetUp)
        edtNotePangDetail.setText(mArgs.pangDetailInfo.note)
    }

    //
    private fun changeFavIcon(_favStatus: Int) {
        activity?.runOnUiThread {
            mFavIconStatus = _favStatus
            if (_favStatus == FAV_IS_ADDED) {
                mMenuItem.icon = resources.getDrawable(R.drawable.ic_fill_like, null)
                showSnackBarShort(view!!, getString(R.string.success_add_fav_pang))
            } else {
                mMenuItem.icon = resources.getDrawable(R.drawable.ic_stroke_like, null)
                showSnackBarShort(view!!, getString(R.string.success_del_fav_pang))
            }
        }
    }

    //
    private fun setErrorChangeFavIcon(_favStatus: Int) {
        activity?.runOnUiThread {
            mFavIconStatus = _favStatus
            if (_favStatus == FAV_IS_ADDED) {
                mMenuItem.icon = resources.getDrawable(R.drawable.ic_fill_like, null)
                showSnackBarShort(view!!, getString(R.string.failed_del_fav_pang))
            } else {
                mMenuItem.icon = resources.getDrawable(R.drawable.ic_stroke_like, null)
                showSnackBarShort(view!!, getString(R.string.failed_add_fav_pang))
            }
        }
    }

    /**
     * API
     */
    // パン参加
    private fun callJoinPang() {
        val memberSeq = PangApp.sMemberSeq
        val reqPangJoin =
            ReqPangJoin(mArgs.pangDetailInfo.seq, memberSeq)
        val call: Call<String> = ApiClient.providePangApi().callJoinPang(reqPangJoin)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.code() == 200) {
                    response.body()?.let {
                        GoLib.goPangMainList(view!!)
                    }
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }

    // パンお気に入り追加
    private fun callAddFavPang() {
        val memberSeq = PangApp.sMemberSeq
        val reqAddFavPang =
            ReqFavPang(memberSeq, mArgs.pangDetailInfo.seq)
        val call: Call<String> = ApiClient.providePangApi().callAddFavPang(reqAddFavPang)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.code() == 200) {
                    response.body()?.let {
                        changeFavIcon(FAV_IS_ADDED)
                    }
                } else {
                    setErrorChangeFavIcon(FAV_IS_DELETED)
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                setErrorChangeFavIcon(FAV_IS_DELETED)
                Logger.d(t.stackTrace)
            }
        })

    }

    // パンお気に入り削除
    private fun callDelFavPang() {
        val memberSeq = PangApp.sMemberSeq
        val pangSeq = mArgs.pangDetailInfo.seq
        val call: Call<ResponseBody> = ApiClient.providePangApi().callDelFavPang(memberSeq, pangSeq)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() == 200) {
                    response.body()?.let {
                        changeFavIcon(FAV_IS_DELETED)
                    }
                } else {
                    setErrorChangeFavIcon(FAV_IS_ADDED)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                setErrorChangeFavIcon(FAV_IS_ADDED)
                Logger.d(t.stackTrace)
            }
        })
    }
}

// TODO Add pang detail photos layout
// TODO Add pang maker info layout

