package com.together.togetherv2kt.ui.frag


import android.content.Context
import android.os.Bundle
import android.view.*
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.BaseFragment
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.DateLib
import com.together.togetherv2kt.lib.DialogLib
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.lib.PrefsLib
import com.together.togetherv2kt.data.remote.model.response.ResAttendance
import kotlinx.android.synthetic.main.fragment_my_page.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * My Page 画面
 * 出席管理
 * パン修正管理
 * 会員情報修正管理
 */
class MyPageFragment : BaseFragment() {

    lateinit var mContext: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.invalidateOptionsMenu()
        return inflater.inflate(R.layout.fragment_my_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callGetAttendanceDataApi()
        setBtnPangsEdit()
        //setBtnPangsFav()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_my_page, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_logout_my_page -> {
                val btnYes = R.string.yes
                val btnNo = R.string.no
                val msg = R.string.dl_msg_do_logout
                val resArr = intArrayOf(msg, btnYes, btnNo)

                DialogLib.show2BtnDialog(mContext, resArr, doLogout)
            }
            R.id.menu_modify_profile -> {
                GoLib.goProfile(view!!)
            }
        }
        return super.onOptionsItemSelected(item)

    }

    private fun setBtnPangsEdit() {
        btnGoPangsEditList.setOnClickListener {
            GoLib.goListPangEdit(it)
        }
    }

    private fun setBtnPangsFav() {
        /* pangsFav.setOnClickListener {
             GoLib.goPangFavList(it)
         }*/
    }

    private fun setTxtAttendanceInfo(_resAttendData: ResAttendance) {
        /*     tvAttendancePlaceName.text = _resAttendData.placeNameMain
             tvAttendanceNum.text = _resAttendData.numMain.toString()
             tvDateAtd.text = DateLib.formatDateTimeShow(DATE_FORMAT, _resAttendData.latestAttendanceMain)*/

        //TODO M sub place
        if (!_resAttendData.placeNameSub.isNullOrBlank()) {
            _resAttendData.placeNameSub
            _resAttendData.numSub.toString()
            DateLib.formatDateTimeShow(DATE_FORMAT, _resAttendData.latestAttendanceSub)
        }
    }


    /**
     * API
     */
    // 出席情報取得
    private fun callGetAttendanceDataApi() {
        val guidMember = PrefsLib.getMemberGuid(mContext)
        val call: Call<ResAttendance>? = ApiClient.providePangApi().callGetAttendanceData(guidMember)
        call?.enqueue(object : Callback<ResAttendance> {
            override fun onResponse(call: Call<ResAttendance>, response: Response<ResAttendance>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        setTxtAttendanceInfo(it)
                    }
                }
            }

            override fun onFailure(call: Call<ResAttendance>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }

    //TODO M 参加ボタン押した場合、参加人情報 + パン情報API
    private val doLogout: () -> Unit = {
        PrefsLib.setAutoLoginVal(mContext, Const.AUTO_LOGIN_FALSE)
        GoLib.goAccount(mContext)
    }
}

//TODO 회원가입시 등록한 가까운 역 수정할 수 있는 기능
//TODO
