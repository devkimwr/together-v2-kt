package com.together.togetherv2kt.ui.frag


import android.Manifest
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle

import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import android.view.*
import android.widget.AdapterView
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.BaseFragment
import com.together.togetherv2kt.custom.RegistrationPlaceAdapter
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.lib.*
import com.together.togetherv2kt.data.remote.model.request.ReqPangAdd
import kotlinx.android.synthetic.main.fragment_pang_add.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.together.togetherv2kt.lib.KeyBoardLib.hideKeyBoard
import com.together.togetherv2kt.data.remote.model.response.ResPlaceNames
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import java.io.File
import java.io.IOException

const val TXT_DATE = "date"
const val TXT_TIME = "time"
const val TXT_PLACE = "place"
const val TXT_NOTE = "note"
const val CHOSE_IMG_GALLERY = 100

/**
 * Pang Register Screen
 */
@RuntimePermissions
class PangAddFragment : BaseFragment() {

    private lateinit var mPangAddMenuItem: MenuItem
    private var mAddPangData = mutableMapOf(TXT_DATE to "", TXT_TIME to "", TXT_PLACE to "", TXT_NOTE to "")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // 메뉴 다시 그림
        activity?.invalidateOptionsMenu()
        return inflater.inflate(R.layout.fragment_pang_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTimePicker()
        setDatePicker()
        setPlaceTxtWatcher()
        setNoteTxtWatcher()
        setClickAddPang()
        callGetPlaceNameRegistered()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_add, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        mPangAddMenuItem = menu.findItem(R.id.menu_register)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_register -> callPangAdd(gatherReqPangAddData())
            android.R.id.home -> hideKeyBoard()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CHOSE_IMG_GALLERY -> {
                if (data != null) {
                    val contentURI = data.data
                    try {
                        val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, contentURI)
                        saveImageWithPermissionCheck(bitmap)
                        setImage(bitmap)
                    } catch (e: IOException) {
                        Logger.d(e)
                    }
                }
            }
        }
    }

    /**
     * FUNCTION
     */

    // Spinner
    private fun initSpnRegisterPlace(_list: List<String>) {
        spnRegisterPlace.adapter = RegistrationPlaceAdapter(context!!, R.layout.v_spn_register_place, _list)
        spnRegisterPlace.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }
        }
    }

    // TimePicker
    private fun setTimePicker() {
        tvTimePangAdd.setOnClickListener {
            val dlTimePicker = DialogLib.makeTimePicker(it.context, timeListener)
            dlTimePicker.show()
            mPangAddMenuItem.isEnabled = !ValidLib.checkEmptyMap(mAddPangData)
        }
    }

    // DatePicker
    private fun setDatePicker() {
        tvDatePangAdd.setOnClickListener {
            val dlDatePicker = DialogLib.makeDatePicker(it.context, dateListener)
            // 현재 일자 이후 표시
            dlDatePicker.datePicker.minDate = System.currentTimeMillis() - 1000
            dlDatePicker.show()
            mPangAddMenuItem.isEnabled = !ValidLib.checkEmptyMap(mAddPangData)
        }
    }

    // Place TextWatcher
    private fun setPlaceTxtWatcher() {
        edtPlaceMeetUpAdd.addTextChangedListener {
            if (it.isNullOrBlank()) {
                mAddPangData[TXT_PLACE] = ""
            } else {
                mAddPangData[TXT_PLACE] = it.toString()
            }

            mPangAddMenuItem.isEnabled = !ValidLib.checkEmptyMap(mAddPangData)
        }
    }

    // Note TextWatcher
    private fun setNoteTxtWatcher() {
        edtNotePangAdd.addTextChangedListener {
            if (it.isNullOrBlank()) {
                mAddPangData[TXT_NOTE] = ""
            } else {
                mAddPangData[TXT_NOTE] = it.toString()
            }

            mPangAddMenuItem.isEnabled = !ValidLib.checkEmptyMap(mAddPangData)
        }
    }

    // Gather pang add data for sending
    private fun gatherReqPangAddData(): ReqPangAdd {
        val mSeq = PangApp.sMemberSeq
        val memberID = PrefsLib.getMemberGuid(context!!)
        val reqPlace = mAddPangData[TXT_PLACE] ?: ""
        val reqNote = mAddPangData[TXT_NOTE] ?: ""
        val dateTime4Parse = "${mAddPangData[TXT_DATE]}${mAddPangData[TXT_TIME]}"
        val parseDateTime = DateLib.parseDateTime(dateTime4Parse)
        val reqDateTime = DateLib.formatDateTime(parseDateTime)
        return ReqPangAdd(
            mSeq,
            memberID,
            reqDateTime,
            reqPlace,
            reqNote
        )
    }


    //TODO Add image function
    private fun setImage(_bitmap: Bitmap) {
        Glide.with(this).load(_bitmap).into(ivMainPangAdd)
    }

    // Show gallery
    private fun setClickAddPang() {
        ivMainPangAdd.setOnClickListener {
            //TODO 클릭후 갤러리 앱 콜.
            chooseImgFromGallery()
        }
    }

    // Gallery startActivityForResult
    private fun chooseImgFromGallery() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, CHOSE_IMG_GALLERY)
    }

    //TODO Check logic
    // Image saving
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun saveImage(myBitmap: Bitmap) {
        val img = ImgLib.saveImage(context!!, myBitmap)
        //TODO 두개의 사진 저장?
        val list = mutableListOf<File>()
        list.add(img)
        list.isNotEmpty()
        Logger.d(img)
    }

    /**
     * API
     */
    // Adding pang
    private fun callPangAdd(_reqPangAdd: ReqPangAdd) {
        val call: Call<String> = ApiClient.providePangApi().callAddPang(_reqPangAdd)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                response.isSuccessful.let {
                    val direct = PangAddFragmentDirections.actionPangAddFragToPangListFrag()
                    findNavController().navigate(direct)
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }

    // Get place name for spinner
    private fun callGetPlaceNameRegistered() {
        val mSeq = PangApp.sMemberSeq
        val call: Call<List<ResPlaceNames>> = ApiClient.providePangApi().getPlaceNameSignedUp(mSeq)
        call.enqueue(object : Callback<List<ResPlaceNames>> {
            override fun onResponse(
                call: Call<List<ResPlaceNames>>,
                response: Response<List<ResPlaceNames>>
            ) {
                response.isSuccessful.let {
                    response.body()?.let { list ->
                        val gotPlaceNameList = listOf(list[0].placeNameMn, list[0].placeNameSb)
                        initSpnRegisterPlace(gotPlaceNameList)
                    }
                }
            }

            override fun onFailure(call: Call<List<ResPlaceNames>>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })
    }

    /**
     * INTERFACE
     */
    // Date Lsn
    private val dateListener = OnDateSetListener { _, year, month, dayOfMonth ->
        val convertedMonth = TextLib.makeTwoDigit(month + 1)
        val convertedDayOfMonth = TextLib.makeTwoDigit(dayOfMonth)
        val resultTxt = "$year$yearDelimiter$convertedMonth$monthDelimiter$convertedDayOfMonth$dayOfMonthDelimiter"
        tvDatePangAdd.text = resultTxt
        mAddPangData[TXT_DATE] = "$year$convertedMonth$convertedDayOfMonth"
    }

    //Time Lsn
    private val timeListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
        val convertedHour = TextLib.makeTwoDigit(hourOfDay)
        val convertedMinute = TextLib.makeTwoDigit(minute)
        val resultTxtTime = "$convertedHour$hourDelimiter$convertedMinute$minuteDelimiter"
        tvTimePangAdd.text = resultTxtTime
        mAddPangData[TXT_TIME] = "$convertedHour$convertedMinute"
    }
}