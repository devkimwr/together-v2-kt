package com.together.togetherv2kt.ui.frag


import android.Manifest
import android.os.Bundle
import android.widget.Toast

import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.together.togetherv2kt.R
import android.annotation.SuppressLint
import com.together.togetherv2kt.common.Const
import android.view.*
import androidx.appcompat.widget.SearchView
import com.google.android.gms.location.*
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.lib.GoogleMapLib
import kotlinx.android.synthetic.main.fragment_pang_map.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.together.togetherv2kt.PangApp
import com.together.togetherv2kt.common.BaseActivity
import com.together.togetherv2kt.common.BaseFragment
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.data.remote.repo.PangMapFragRepo
import com.together.togetherv2kt.lib.LocationLib
import com.together.togetherv2kt.lib.PrefsLib
import com.together.togetherv2kt.data.remote.model.request.ReqAttendance
import com.together.togetherv2kt.data.remote.model.response.ResStations
import com.together.togetherv2kt.data.remote.model.response.Station
import com.together.togetherv2kt.lib.GoLib
import com.together.togetherv2kt.ui.act.MainActivity
import com.together.togetherv2kt.viewmodel.PangMapViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * パンマップ
 */
@RuntimePermissions
class PangMapFragment : BaseFragment(),
    GoogleMap.OnMapClickListener,
    GoogleMap.OnMarkerClickListener,
    GoogleMap.OnInfoWindowClickListener {

    private val mFusedLocationClient
            by lazy { LocationServices.getFusedLocationProviderClient(context!!) }
    private val pangMapViewModel
            by lazy { ViewModelProviders.of(activity!!).get(PangMapViewModel::class.java) }

    private val mLocationRequest by lazy { LocationLib(context!!).makeLocationReq() }
    private val mLocationCallBack by lazy { makeLocationCb() }

    private val mBaseAct by lazy { activity as BaseActivity }
    private val mMemberSeq by lazy { PangApp.sMemberSeq }
    // 출석시간 비교용 캘린더
    private val mCalendar by lazy { GregorianCalendar.getInstance() }
    // 현재위치 저장용
    private lateinit var mLocation: Location
    private lateinit var testDate: Date
    private lateinit var mGMap: GoogleMap
    private lateinit var mContext: Context
    private var mLat: Double = 0.00
    private var mLng: Double = 0.00
    private var mIsGpsOn = false

    val mDl by lazy {
        MaterialDialog(context!!)
            .customView(R.layout.dialog_item_progress_bar)
            .show {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
        } else {
            throw RuntimeException("$context must implement OnCheckingGpsOnLsn")
        }
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.run { invalidateOptionsMenu() }
        //  callGetMainPlaceNameOfMember(mMemberSeq)
        val rootView = inflater.inflate(R.layout.fragment_pang_map, container, false)
        asyncGoogleMapWithPermissionCheck()
        testDate = GregorianCalendar.getInstance().time
        mLocation = Location("")
        mBaseAct.supportActionBar?.hide()
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFbMyLocation()
        setFbMarkClosestStations()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pang_map, menu)
        setSearchView(menu)
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        marker?.run {
            showInfoWindow()
        }

        return true
    }

    override fun onInfoWindowClick(marker: Marker?) {
        marker?.run {
            val stationInfo = this.tag as Station
            callRegisterAttendance(makeReqAttendance(stationInfo))
            hideInfoWindow()
            // Station Board로 이동
            GoLib.goStationsBoard(requireView(), stationInfo)
        }
    }

    override fun onMapClick(p0: LatLng?) {
        Toast.makeText(mContext, "MAP CLICK $p0", Toast.LENGTH_SHORT).show()
    }

    // Google Mapの同期化
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun asyncGoogleMap() {
        mDl.show()
        val googleMapFrag = SupportMapFragment.newInstance()
        googleMapFrag.getMapAsync { googleMap ->
            mDl.dismiss()
            initGoogleMapSettings(googleMap)
            getDeviceLocation()
        }

        childFragmentManager.beginTransaction().replace(R.id.layoutPangMap, googleMapFrag).commit()
    }

    // Google Map 関連設定
    @SuppressLint("MissingPermission")
    private fun initGoogleMapSettings(_gMap: GoogleMap) {
        _gMap.run {
            mGMap = _gMap
            isMyLocationEnabled = true
            uiSettings.isMyLocationButtonEnabled = false
            setOnMarkerClickListener(this@PangMapFragment)
            setOnMapClickListener(this@PangMapFragment)
            setOnInfoWindowClickListener(this@PangMapFragment)
        }
    }

    private fun setDefaultMarker(_latLng: LatLng) {
        mGMap.addMarker(GoogleMapLib.makeMapMarker(_latLng, "Current", _markerColor = 1))
        GoogleMapLib.moveCamera(mGMap, _latLng, Const.MAP_ZOOM_IN_MIN)
    }

    private fun getDefaultLatLng(): LatLng {
        when (Locale.getDefault().language) {
            Const.KO_LOCALE_CODE -> {
            }

            Const.JP_LOCALE_CODE -> {
                return LatLng(35.68123620000001, 139.76712480000003)
            }

            Const.EN_LOCALE_CODE -> {
            }
        }

        return LatLng(37.387474, -122.05754339999999)
    }

    private fun setGpsOn(): Boolean {
        return LocationLib(context!!).turnGPSOn()
    }

    // ボタン現在位置
    @SuppressLint("MissingPermission")
    private fun setFbMyLocation() {
        fbMyLocation.setOnClickListener { _ ->
            mDl.show()
            if (setGpsOn()) {
                getDeviceLocation()
            } else {
                setGpsOn()
            }

            pangMapViewModel.isGpsOn.observe(this, Observer {
                if (it) {
                    getDeviceLocation()
                }
            })
        }
    }

    // TODO 허루에 한번만 클릭 가능 로직 구현
    //ボタン最寄り駅マーカ表示
    private fun setFbMarkClosestStations() {
        fbAttend.setOnClickListener {
            val currentDate = mCalendar.time
            //TODO API 에서 과거 출석일 + 출석 티켓 수를 가지고 옴.
            if (canRegisterAttendance(testDate, currentDate, 1)) {
                callGetClosestStations(LatLng(mLocation.latitude, mLocation.longitude))
            } else {
                showSnackBarShort(it, "you can't attendance, please buy ticket for attendance.")
            }
        }
    }

    // 現在のユーザ位置
    @SuppressLint("MissingPermission")
    fun getDeviceLocation() {
        mFusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            mDl.dismiss()
            if (location != null) {
                mLocation = location
                val latLng = LatLng(location.latitude, location.longitude)
                GoogleMapLib.animateCamera(mGMap, latLng, Const.DEFAULT_ZOOM)
            } else {
                setGpsOn()
                //mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallBack, null)
            }
        }.addOnFailureListener {
            mDl.dismiss()
            Logger.d("getDeviceLocation Failed")
        }
    }

    // Search View
    private fun setSearchView(menu: Menu) {
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        searchView.queryHint = getString(R.string.hint_search_place)
        searchView.apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    hideKeyboard(mContext, searchView)
                    callGetStationData(processStationSearchWord(query))
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    return false
                }
            })
        }
    }

    private fun makeLocationCb(): LocationCallback {
        return object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                locationResult?.run {
                    mLocation = locations[0]
//                    mLat = locations[0].latitude
//                    mLng = locations[0].longitude
                }

                if (mFusedLocationClient != null) {
                    mFusedLocationClient.removeLocationUpdates(mLocationCallBack)
                }
            }
        }
    }

    // 駅APIをCallする前に”駅”文字を消去
    fun processStationSearchWord(_query: String): String {
        // 유저가 입력한 역이름에 "역"이 포함 되어 있을 경우 "역"을 삭제 후 API 에 쓰일 스트링에 저장
        return if (_query.contains(getString(R.string.station))) {
            _query.replace(getString(R.string.station), "")
        } else {
            _query
        }
    }

    // 駅情報のMarkerの追加
    private fun addMarker4Stations(_resStation: ResStations) {
        _resStation.stations?.stationInfo?.forEach {
            val latLng = LatLng(it.y, it.x)
            val m = mGMap.addMarker(GoogleMapLib.makeMapMarker(latLng, "${it.name}/${it.line}", it.distance))
            m.tag = it
            GoogleMapLib.animateCamera(mGMap, latLng, Const.MAP_ZOOM_IN_MIN)
        }
    }

    //
    private fun makeReqAttendance(_stationInfo: Station): ReqAttendance {
        val guid = PrefsLib.getMemberGuid(context!!)
        return ReqAttendance(guid, _stationInfo)
    }

    //TODO 하루에 한번 출석체크 가능 혹은 횟수를 제한가능하개 하는 함수
    // 현재시간 , 유저가 하루에 출석체크 할 수 있는 수
    /**
     * @param attendedDate 멤버가 과거 출석한 날짜
     * @param currentDate 현재 날짜
     * @param numAttendance2 멤버가 가지고 있는 출석 티켓 수
     *
     * @return 과거 출석일 보다 미래 혹은
     */
    private fun canRegisterAttendance(attendedDate: Date, currentDate: Date, numAttendance2: Int): Boolean {
        // 현재시간과 출석체크 성공시간을 비교(출석했던 시간보다 현재시간이 클 경우에만 출석 가능)
        val resultCompared = attendedDate.compareTo(currentDate)
        if (resultCompared < 0 && numAttendance2 > 0) {
            return true
        }

        return false
    }

    /**
     * INTERFACE
     */

    /**
     * API
     */

    // SerchView
    private fun callGetStationData(_stationName: String) {
        val call: Call<ResStations> = ApiClient.provideStationApi().callGetStationData(_stationName)
        call.enqueue(object : Callback<ResStations> {
            override fun onResponse(call: Call<ResStations>, response: Response<ResStations>) {
                mDialog.dismiss()
                if (response.isSuccessful) {
                    response.body()?.run {
                        addMarker4Stations(this)
                    }
                }
            }

            override fun onFailure(call: Call<ResStations>, t: Throwable) {
                mDialog.dismiss()
                Logger.d(t.stackTrace)
            }
        })
    }

    // 最寄り駅情報取得後、マーカー表示.
    private fun callGetClosestStations(_latLng: LatLng) {
        //  mDialog.show()
        val repo = PangMapFragRepo.callGetClosestStations(_latLng)
        repo.observe(this, Observer {
            mDialog.dismiss()
            addMarker4Stations(it)
        })
    }

    // 最寄り駅出席情報登録
    private fun callRegisterAttendance(_attendanceInfo: ReqAttendance) {
        val repo = PangMapFragRepo.callRegisterAttendance(_attendanceInfo)
        repo.observe(this, Observer {
            mDialog.dismiss()
            if (it) {
                mGMap.clear()

            } else {
                showSnackBarShort(view!!, "Registration is Failed!")
            }
        })
    }

    // TODO 등록된 근처역 정보 취득
    private fun callGetMainPlaceNameOfMember(_memberSeq: Int) {
        mDialog.show()
        val repo = PangMapFragRepo.callGetMainPlaceNameOfMember(_memberSeq)
        repo.observe(this, Observer {
            mDialog.dismiss()
            Logger.d(it)
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mBaseAct.supportActionBar?.show()
        Logger.d("MAP")
    }
}

//TODO 검색한 역 게시판에서 검색 화면으로 돌아오면 검색한 결과가 사라짐.
//TODO 一日一回だけ出席可能？
//TODO 現在ユーザーの位置マーカーはほかの色？
//TODO 일본 이외의 지역은 출석체크 어떻게?
//TODO ERROR GPS off 에서 on 으로 바꾼뒤 현재 위치 검색하면 위치 정보 0을 반환.

