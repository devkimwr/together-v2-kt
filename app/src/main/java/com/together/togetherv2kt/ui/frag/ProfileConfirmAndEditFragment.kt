package com.together.togetherv2kt.ui.frag


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.together.togetherv2kt.R
import com.together.togetherv2kt.common.BaseFragment

class ProfileConfirmAndEditFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_confirm_and_edit, container, false)
    }
}