package com.together.togetherv2kt.data.remote.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

data class ResAttendance(
    @SerializedName("place_name_mn") val placeNameMain: String,
    @SerializedName("attend_num_mn") val numMain: Int,
    @SerializedName("latest_date_mn") val latestAttendanceMain: Date,
    @SerializedName("place_name_sub") val placeNameSub: String,
    @SerializedName("attend_num_sub") val numSub: Int,
    @SerializedName("latest_date_sub") val latestAttendanceSub: Date
)