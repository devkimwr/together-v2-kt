package com.together.togetherv2kt.data.remote.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class ResStations(
    // 駅の情報の一覧
    @SerializedName("response")
    val stations: Stations? = null
)

data class Stations(
    // 駅の情報
    @SerializedName("station")
    val stationInfo: List<Station>
)

@Parcelize
data class Station(
    // 駅名
    @SerializedName("name")
    val name: String,
    // 前の駅名 （始発駅の場合は null）
    @SerializedName("prev")
    val prev: String,
    // 次の駅名 （終着駅の場合は null）
    @SerializedName("next")
    val next: String,
    // 駅の経度 longitude（世界測地系）
    @SerializedName("x")
    val x: Double,
    // 駅の緯度 latitude（世界測地系）
    @SerializedName("y")
    val y: Double,
    // 指定の場所から最寄駅までの距離 （精度は10ｍ）
    @SerializedName("distance")
    val distance: String = "",
    // 駅の郵便番号
    @SerializedName("postal")
    val postal: String,
    // 駅の存在する都道府県名
    @SerializedName("prefecture")
    val prefecture: String,
    // 駅の存在する路線名
    @SerializedName("line")
    val line: String
) : Parcelable