package com.together.togetherv2kt.data.remote.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResLogin(
    @SerializedName("seq") val memberSeq: Int,
    @SerializedName("guid_member") val memberGuid: String
) : Parcelable