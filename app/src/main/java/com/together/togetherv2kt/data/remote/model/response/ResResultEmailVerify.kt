package com.together.togetherv2kt.data.remote.model.response

import com.google.gson.annotations.SerializedName

data class ResResultEmailVerify(@SerializedName("email") val email: String)