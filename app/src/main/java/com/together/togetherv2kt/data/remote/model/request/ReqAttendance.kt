package com.together.togetherv2kt.data.remote.model.request

import com.together.togetherv2kt.data.remote.model.response.Station

data class ReqAttendance(val guid: String, val stationInfo: Station)