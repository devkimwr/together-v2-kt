package com.together.togetherv2kt.data.remote

import android.content.Context
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

object ApiClient {
    var BASE_URL = "http://34.85.119.58:3000"
    var STATION_API_URL = "http://express.heartrails.com"

    // 認証情報を取得するAPI Client生成
/*    fun provideAuthApi(): AuthApiService = Retrofit.Builder()
        .baseUrl(API_DOMAIN)
        .client(provideOkHttpClient(provideLoggingInterceptor()))
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AuthApiService::class.java)*/

    // Route ZのApi Client生成
    fun providePangApi(): ApiServicePang = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(
            provideOkHttpClient(provideLoggingInterceptor())
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiServicePang::class.java)

    // Route ZのApi Client生成
    fun provideStationApi(): ApiServiceStation = Retrofit.Builder()
        .baseUrl(STATION_API_URL)
        .client(
            provideOkHttpClient(provideLoggingInterceptor())
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiServiceStation::class.java)


    private fun provideOkHttpClient(
        interceptor: HttpLoggingInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .run {
            /*   if (null != authInterceptor) {
                   addInterceptor(authInterceptor)
               }*/
            connectTimeout(10, TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
            writeTimeout(10, TimeUnit.SECONDS)
            addInterceptor(interceptor)
            build()
        }

    // Route Z用のInterceptor
    private fun provideLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

    // Auth用のInterceptor
/*    private fun provideAuthInterceptor(provider: AuthDataProvider): AuthInterceptor {
        val token = provider.token ?: throw IllegalStateException("authToken cannot be null.")
        val serviceCode = provider.serviceCode
                ?: throw IllegalStateException("serviceCode cannot be null.")
        return AuthInterceptor(token, serviceCode)
    }*/

    // SharedPreferenceから認証情報を取得
/*
    private fun provideAuthTokenProvider(context: Context): AuthDataProvider =
        AuthDataProvider(context.applicationContext)
*/

/*    // 認証用のInterceptor生成
    internal class AuthInterceptor(private val token: String, private val serviceCode: String) : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain)
                : Response = with(chain) {

            // 認証APIを叩くときのHeader情報追加
            val newRequest = request().newBuilder().run {
                addHeader(ROUTEZ_SERVICE_TOKEN_TXT, token)
                addHeader(ROUTEZ_SERVICE_CODE_TXT, serviceCode)
                addHeader("Content-Type", "application/octet-stream")
                build()
            }

            proceed(newRequest)
        }
    }*/

}