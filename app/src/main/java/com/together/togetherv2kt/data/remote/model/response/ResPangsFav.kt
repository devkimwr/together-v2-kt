package com.together.togetherv2kt.data.remote.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class ResPangsFav(
    val seq: Int,
    @SerializedName("m_seq") val mSeq: String,
    @SerializedName("date_meet_up") val dateMeetUp: Date,
    @SerializedName("place_meet_up") val placeMeetUp: String,
    val note: String,
    @SerializedName("photo_seq") val photoSeq: Int
) : Parcelable