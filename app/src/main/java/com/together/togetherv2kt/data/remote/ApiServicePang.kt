package com.together.togetherv2kt.data.remote

import com.together.togetherv2kt.data.remote.model.request.*
import com.together.togetherv2kt.data.remote.model.response.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.GET


interface ApiServicePang {

    /**
     * Login
     */


    /**
     * ACCOUNT
     */
    // Email送信(会員加入時、認証番号を送信するため)
    @FormUrlEncoded
    @POST("validation/verify/mail")
    fun callSendVerifyEmail(
        @Field("email") _memberEmail: String
    ): Call<ResponseBody>

    // メール認証番号確認
    @POST("validation/verify/check")
    fun callCheckVerifyNum(
        @Body reqCheckVerifyNum: ReqCheckVerifyNum
    ): Call<ResResultEmailVerify>

    // 会員情報登録.
    @POST("sign-up")
    fun callSignUp(
        @Body _reqSignUp: ReqSignUp
    ): Call<String>

    // ログイン
    @POST("login")
    fun callLogin(@Body reqLogin: ReqLogin): Call<ResLogin>

    // 会員のseq取得
    // post + stringを渡すときは @Fieldを使用
    @FormUrlEncoded
    @POST("account/member-seq")
    fun callGetMemberSeq(
        @Field("memberGuid") _memberGuid: String
    ): Call<Int>

    // 会員最寄り駅取得
    // {} 에 써있는 이름으로 서버에서 path 값을 받음
    @GET("account/place-name/{m_seq}")
    fun getPlaceNameSignedUp(@Path("m_seq") mSeq: Int): Call<List<ResPlaceNames>>

    @GET("account/main-place-name/{m_seq}")
    fun getMainPlaceName(@Path("m_seq") mSeq: Int): Call<ResponseBody>

    /**
     * PANG
     */
    // リストデータ取得
    @GET("pangs/{m-seq}")
    fun callGetPangs(
        @Path("m-seq") mSeq: Int,
        @Query("order") orderStatus: Int
    ): Call<List<ResPangs>>

    // 自分が作ったパンリストデータ取得
    @GET("pangs/{m_seq}")
    fun callGetPangsEdit(@Path("m_seq") mSeq: Int): Call<List<ResPangsEdit>>

    // 参加
    @POST("pangs/join")
    fun callJoinPang(
        @Body reqPangJoin: ReqPangJoin
    ): Call<String>

    // 登録
    @POST("pangs")
    fun callAddPang(
        @Body pangAdd: ReqPangAdd
    ): Call<String>

    // 削除
    @DELETE("pangs")
    fun callDelPang(
        @Field("pangSeq") _pangSeq: Int
    ): Call<ResponseBody>

    // 修正
    @PATCH("pangs")
    fun callUpdatePang(
        @Body _reqPangUpdate: ReqPangUpdate
    ): Call<ResponseBody>


    /**
     * SCHEDULE
     */
    //TODO ing Scheduleのリスト取得
    @GET("schedules/{m_seq}")
    fun callGetSchedules(
        @Path("m_seq") _memberSeq: Int
    ): Call<List<ResSchedules>>

    // パン参加取り消し
    @DELETE("schedules/{pang-seq}/{member-seq}")
    fun callCancelJoinPang(
        @Path("pang-seq") _pangSeq: Int,
        @Path("member-seq") _joinMemberSeq: Int
    ): Call<ResponseBody>


    /**
     * LIKE PANG
     */
    //TODO パンお気に入りリストデータ
    // リストデータ取得
    @GET("fav/{member-seq}")
    fun callGetFavPangList(@Path("member-seq") memberSeq: Int): Call<List<ResPangsFav>>

    // パンお気に入り登録
    @POST("fav")
    fun callAddFavPang(@Body reqFavPang: ReqFavPang): Call<String>

    // パンお気に入り削除
    @DELETE("fav/{member_seq}/{pang_seq}")
    fun callDelFavPang(
        @Path("member_seq") memberSeq: Int,
        @Path("pang_seq") pangSeq: Int
    ): Call<ResponseBody>

    /**
     * ATTENDANCE
     */
    // 出席情報取得
    @FormUrlEncoded
    @POST("attendance")
    fun callGetAttendanceData(
        @Field("memberId") memberId: String
    ): Call<ResAttendance>

    //TODO 出席情報登録
    @POST("attendance/registration")
    fun callRegisterAttendance(
        @Body attendanceInfo: ReqAttendance
    ): Call<String>
}