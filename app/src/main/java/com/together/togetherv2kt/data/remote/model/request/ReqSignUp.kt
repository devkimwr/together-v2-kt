package com.together.togetherv2kt.data.remote.model.request

data class ReqSignUp(
    var guidMember: String = "",
    var name: String = "",
    var email: String = "",
    var pw: String = "",
    var gender: Int = 0,
    var birthDay: String = "",
    var placeMn: String = "",
    var placeSub: String = "",
    var note: String = ""
)