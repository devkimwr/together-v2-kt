package com.together.togetherv2kt.data.remote.model.request

data class ReqPangUpdate(
    val memberSeq: Int,
    val pangSeq: Int,
    val dateMeet: String,
    val placeMeet: String,
    val note: String,
    val photoSeq: Int
)