package com.together.togetherv2kt.data.remote.model.response

import com.google.gson.annotations.SerializedName

data class ResPlaceNames(
    @SerializedName("place_mn") val placeNameMn: String,
    @SerializedName("place_sub") val placeNameSb: String
)