package com.together.togetherv2kt.data.local.entity

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity
data class PangEntity(
    val content: String,
    val dateMeet: Date,
    val placeMeet: String,
    val photosPang: String
) : Parcelable