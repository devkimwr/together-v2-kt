package com.together.togetherv2kt.data.remote.model.request

data class ReqCheckVerifyNum(val email: String, val verifyNum: String)