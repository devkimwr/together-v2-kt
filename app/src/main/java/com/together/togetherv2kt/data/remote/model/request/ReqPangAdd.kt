package com.together.togetherv2kt.data.remote.model.request

data class ReqPangAdd(
    val mSeq: Int,
    val pangMakerId: String = "",
    val dateMeet: String,
    val placeMeet: String,
    val note: String,
    val photoMn: String = "",
    val photoSub: String = ""
)