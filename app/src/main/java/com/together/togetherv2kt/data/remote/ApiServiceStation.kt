package com.together.togetherv2kt.data.remote

import com.together.togetherv2kt.data.remote.model.response.ResStations
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.GET


interface ApiServiceStation {

    /**
     * STATION
     */
    // 駅情報取得
    @GET("/api/json?method=getStations")
    fun callGetStationData(@Query("name") name: String): Call<ResStations>

    @GET("/api/json?method=getStations")
    fun callClosestStations(
        @Query("x") lon: Double,
        @Query("y") lat: Double
    ): Call<ResStations>
}