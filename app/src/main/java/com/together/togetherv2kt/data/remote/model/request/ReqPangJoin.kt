package com.together.togetherv2kt.data.remote.model.request

data class ReqPangJoin(val pangSeq: Int, val memberSeq: Int, val guidToJoin: String = "")