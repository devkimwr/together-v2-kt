package com.together.togetherv2kt.data.remote.model.request

data class ReqFavPang(val memberSeq: Int, val pangSeq: Int)