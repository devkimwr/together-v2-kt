package com.together.togetherv2kt.data.local.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PangListMd(
    val index: Int,
    val content: String,
    val place2Meet: String,
    val date2Meet: String,
    val entryId: String,
    val makerId: String
) : Parcelable
