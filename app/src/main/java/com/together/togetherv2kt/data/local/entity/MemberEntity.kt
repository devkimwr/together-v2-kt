package com.together.togetherv2kt.data.local.entity

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class MemberEntity(
    val name: String,
    val gender: Int,
    val birthDay: String
) : Parcelable