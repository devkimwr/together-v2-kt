package com.together.togetherv2kt.data.remote.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.orhanobut.logger.Logger
import com.together.togetherv2kt.data.remote.ApiClient
import com.together.togetherv2kt.data.remote.model.request.ReqAttendance
import com.together.togetherv2kt.data.remote.model.response.ResStations
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object PangMapFragRepo {

    // 最寄り駅出席情報登録
    fun callGetMainPlaceNameOfMember(_mSeq: Int): LiveData<String> {
        val placeNameOfMember = MutableLiveData<String>()
        val call: Call<ResponseBody> = ApiClient.providePangApi().getMainPlaceName(_mSeq)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    //TODO not toString(), should string()
                    val body = response.body()?.string()
                    placeNameOfMember.postValue(body)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.stackTrace
                Logger.d(t.stackTrace)
            }
        })

        return placeNameOfMember
    }

    fun callGetClosestStations(_latLng: LatLng): LiveData<ResStations> {
        val closestStationData = MutableLiveData<ResStations>()

        val call: Call<ResStations> =
            ApiClient.provideStationApi().callClosestStations(_latLng.longitude, _latLng.latitude)
        call.enqueue(object : Callback<ResStations> {
            override fun onResponse(call: Call<ResStations>, response: Response<ResStations>) {
                if (response.isSuccessful) {
                    closestStationData.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<ResStations>, t: Throwable) {
                Logger.d(t.stackTrace)
            }
        })

        return closestStationData
    }


    fun callRegisterAttendance(_attendanceInfo: ReqAttendance): LiveData<Boolean> {
        val isSuccess = MutableLiveData<Boolean>()
        val call: Call<String> = ApiClient.providePangApi().callRegisterAttendance(_attendanceInfo)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.isSuccessful) {
                    isSuccess.postValue(true)
                    response.body()
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                isSuccess.postValue(false)
                Logger.d(t.stackTrace)
            }
        })

        return isSuccess
    }
}