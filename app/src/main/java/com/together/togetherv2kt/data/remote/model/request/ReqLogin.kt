package com.together.togetherv2kt.data.remote.model.request

data class ReqLogin(
    val email: String,
    val pw: String,
    val autoLoginStatus: Int
)