package com.together.togetherv2kt.lib

import android.text.Editable

/**
 * 有効性検査
 */
object ValidLib {

    /**
     * ボタンの有効化
     * @param _arr4Check: array to check
     * @return if all ok, true not false
     */
    fun isEmptyEdtArr(_arr4Check: IntArray): Boolean {
        _arr4Check.forEach {
            // 0:empty , 1: not empty
            if (it == 0) {
                return true
            }
        }
        return false
    }

    fun checkEmptyMap(_map4Check: MutableMap<String, String>): Boolean {
        _map4Check.forEach {
            // isNullOrEmpty 는 "" true, isNullOrBlank 는 "" false
            if (it.value.isNullOrEmpty()) {
                return true
            }
        }
        return false
    }

    // 이메일 형식이 맞는지 검사
    private fun checkEmailFormat(_input: Editable): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(_input).matches()
    }

    /**
     * Show email error to textInputLayout
     * @param _input 検査するEditText
     * @return 0 Empty Email
     * @return 1 Not Email Format
     * @return 9 Right Email Format
     */
    fun checkEdtEmailError(_input: Editable): Int {
        when {
            _input.isNullOrEmpty() -> return 0
            !checkEmailFormat(_input) -> return 1
        }
        return 9
    }
}