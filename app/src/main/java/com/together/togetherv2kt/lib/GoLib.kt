package com.together.togetherv2kt.lib

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.together.togetherv2kt.data.remote.model.response.ResPangsEdit
import com.together.togetherv2kt.data.remote.model.response.ResPangsFav
import com.together.togetherv2kt.data.remote.model.response.Station
import com.together.togetherv2kt.ui.act.AccountActivity
import com.together.togetherv2kt.ui.act.MainActivity
import com.together.togetherv2kt.ui.frag.MyPageFragmentDirections
import com.together.togetherv2kt.ui.frag.PangMapFragmentDirections
import com.together.togetherv2kt.ui.frag.StationsBoardFragmentDirections
import com.together.togetherv2kt.ui.frag.account.SignUpAccountInfoFragmentDirections
import com.together.togetherv2kt.ui.frag.account.SignUpEmailVerifyFragmentDirections
import com.together.togetherv2kt.ui.frag.detail.PangFragmentDirections
import com.together.togetherv2kt.ui.frag.list.PangEditListFragmentDirections
import com.together.togetherv2kt.ui.frag.list.PangFavListFragmentDirections
import com.together.togetherv2kt.ui.frag.list.PangMainListFragmentDirections

object GoLib {

    fun navigateUp(_view: View) {
        _view.findNavController().navigateUp()
    }

    /**
     * FRAGMENT
     */
    fun goSignUpAccount(_view: View, _email: String) {
        val direction = SignUpEmailVerifyFragmentDirections.actionSignUpVerifyEmailToSignUpAccount(_email)
        _view.findNavController().navigate(direction)
    }

    fun goSignUpExtraInfo(_view: View) {
        val direction = SignUpAccountInfoFragmentDirections.actionSignUpAccountToSignUpExtra()
        _view.findNavController().navigate(direction)
    }

    fun goPangAdd(_view: View) {
        val direction = PangMainListFragmentDirections.actionPangListToPangAdd()
        _view.findNavController().navigate(direction)
    }

    fun goPangFavList(_view: View) {
        //TODO M
        val direction = PangMainListFragmentDirections.actionToPangFavListFrag()
        //   _view.findNavController().navigateUp()
        _view.findNavController().navigate(direction)
    }

    fun goPangFav(_view: View, _pangFavInfo: ResPangsFav) {
        val direction = PangFavListFragmentDirections.actionPangFavListToPangFav(_pangFavInfo)
        _view.findNavController().navigate(direction)
    }

    fun goListPangEdit(_view: View) {
        val direction = MyPageFragmentDirections.actionToPangEditListFrag()
        _view.findNavController().navigate(direction)
    }

    fun goPangEdit(_view: View, _pangEditItem: ResPangsEdit) {
        val direction = PangEditListFragmentDirections.actionToPangEditFrag(_pangEditItem)
        _view.findNavController().navigate(direction)
    }

    fun goPangMainList(_view: View) {
        val direction = PangFragmentDirections.actionPangDetailFragToPangListFrag()
        _view.findNavController().navigate(direction)
    }

    //
    fun goStationsBoard(_view: View, _stationInfo: Station) {
        val direction = PangMapFragmentDirections.actionPangMapToStationsBoard(_stationInfo)
        _view.findNavController().navigate(direction)
    }

    //
    fun goPlaceMap(_view: View, _statusName: String) {
        val bundle = bundleOf("stationName" to _statusName)
        val direction = StationsBoardFragmentDirections.actionStationsBoardToPangMap()
        _view.findNavController().navigate(direction.actionId, bundle)
    }

    fun goProfile(_view: View) {
        val direction = MyPageFragmentDirections.actionToProfileFrag()
        _view.findNavController().navigate(direction)
    }

    /**
     * ACTIVITY
     */
    fun goMain(_context: Context) {
        val intent = Intent(_context, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        _context.startActivity(intent)
    }

    fun goAccount(_context: Context) {
        val intent = Intent(_context, AccountActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        _context.startActivity(intent)
    }
}