package com.together.togetherv2kt.lib

import com.together.togetherv2kt.common.Const.SQL_DATETIME_FORMAT
import com.together.togetherv2kt.common.Const.SQL_DATETIME_PARSE_2_MINUTE
import java.text.SimpleDateFormat
import java.util.*

object DateLib {

    fun parseDateTime(_dateTimeString: String): Date {
        return SimpleDateFormat(SQL_DATETIME_PARSE_2_MINUTE, Locale.getDefault()).parse(_dateTimeString)
    }

    fun formatDateTime(_dateTime: Date): String {
        return SimpleDateFormat(SQL_DATETIME_FORMAT, Locale.getDefault()).format(_dateTime)
    }

    fun formatDateTimeShow(_format: String, _dateTime: Date): String {
        return SimpleDateFormat(_format, Locale.getDefault()).format(_dateTime)
    }

    fun getTimeFromDateTimeFormat(_timeFormat: String, _dateTime: Date): String {
        return SimpleDateFormat(_timeFormat, Locale.getDefault()).format(_dateTime)
    }

    fun getDateFromDateTimeFormat(_dateFormat: String, _dateTime: Date): String {
        return SimpleDateFormat(_dateFormat, Locale.getDefault()).format(_dateTime)
    }
}