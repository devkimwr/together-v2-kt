package com.together.togetherv2kt.lib

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions


object GoogleMapLib {

    /**
     * Move Map Camera
     *
     * @param googleMap Google Map
     * @param latLng    LatLng
     * @param zoomLev   Map Zoom Level
     */
    fun animateCamera(googleMap: GoogleMap, latLng: LatLng, zoomLev: Int) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLev.toFloat()))
    }

    fun moveCamera(googleMap: GoogleMap, latLng: LatLng, zoomLev: Int) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLev.toFloat()))
    }

    fun makeMapMarker(_latLng: LatLng, _title: String, _snippet: String = "", _markerColor: Int = 0): MarkerOptions {
        if (_markerColor == 0) {
            return MarkerOptions().position(_latLng).title(_title).snippet(_snippet) // 타이틀.
        }

        return MarkerOptions()
            .position(_latLng)
            .title(_title)
            .snippet(_snippet)
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
    }
}