package com.together.togetherv2kt.lib

import android.content.IntentSender
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.together.togetherv2kt.common.Const
import com.together.togetherv2kt.common.BaseActivity


/**
 * GPS Checking
 *
 * @param <T> Activity
</T> */

class GpsOnChecking<T : BaseActivity>(private val mActivity: T) {

    // LocationSettingsResponse 를 call and if res completed call getLocationSettingsRes(Task<LocationSettingsResponse> res)
    fun settingsRequest() {
        val result =
            LocationServices.getSettingsClient(mActivity).checkLocationSettings(createLocationSettingsRequest())

        result.addOnCompleteListener { task -> getLocationSettingsRes(task) }
    }

    // req 요청할 정보 생성
    private fun createLocationSettingsRequest(): LocationSettingsRequest {
        val builder = LocationSettingsRequest.Builder().addLocationRequest(createLocationRequest())
        builder.setAlwaysShow(true) //this is the key ingredient
        return builder.build()
    }

    private fun createLocationRequest(): LocationRequest {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (30 * 1000).toLong()
        locationRequest.fastestInterval = (5 * 1000).toLong()
        return locationRequest
    }

    // Gps 가 on 인지 확인 -> off 이면  doApiException(ApiException exception) call
    private fun getLocationSettingsRes(res: Task<LocationSettingsResponse>) {
        try {
            val response = res.getResult(ApiException::class.java)
            // All location settings are satisfied. The client can initialize location
            // requests here.
        } catch (exception: ApiException) {
            doApiException(exception)
        }
    }

    // 사용자가 gps 를 꺼났을 경우 해결 가능 하면 다이얼로그 요청
    private fun doApiException(exception: ApiException) {
        when (exception.statusCode) {
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> showDialogCouldBeFixed(exception)
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
            }
        }
        // Location settings are not satisfied. However, we have no way to fix the
        // settings so we won't show the dialog.
    }

    // dialog 를 보여주고 onActivityResult 를 요청
    private fun showDialogCouldBeFixed(exception: ApiException) {
        // Location settings are not satisfied. But could be fixed by showing the
        // user a dialog.
        try {
            // Cast to a resolvable exception.
            val resolvable = exception as ResolvableApiException
            // Show the dialog by calling startResolutionForResult(),
            // and check the result in onActivityResult().

            resolvable.startResolutionForResult(
                mActivity,
                Const.REQUEST_CHECK_SETTINGS
            )
        } catch (e: IntentSender.SendIntentException) {
            // Ignore the error.
        } catch (e: ClassCastException) {
            // Ignore, should be an impossible error.
        }

    }
}


/*
 * [Java] 제네릭(Generic)
 * https://onsil-thegreenhouse.github.io/programming/java/2018/02/17/java_tutorial_1-21/
 * */


