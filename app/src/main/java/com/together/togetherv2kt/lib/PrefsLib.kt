package com.together.togetherv2kt.lib

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.together.togetherv2kt.common.Const.FN_AUTO_LOGIN
import com.together.togetherv2kt.common.Const.FN_MEMBER_GUID
import com.together.togetherv2kt.common.Const.FN_MEMBER_SEQ
import com.together.togetherv2kt.common.Const.KN_IS_AUTO_LOGIN
import com.together.togetherv2kt.common.Const.KN_MEMBER_GUID
import com.together.togetherv2kt.common.Const.KN_MEMBER_SEQ

object PrefsLib {

    private fun getPrefs(_context: Context, _fileName: String): SharedPreferences {
        return _context.getSharedPreferences(_fileName, 0)
    }

    /**
     * AutoLogin
     *  true : 90
     *  false : 10
     */
    fun getAutoLoginVal(_context: Context): Int {
        val prefsIsAutoLogin = getPrefs(_context, FN_AUTO_LOGIN)
        return prefsIsAutoLogin.getInt(KN_IS_AUTO_LOGIN, 0)
    }

    fun getMemberGuid(_context: Context): String {
        var guid = ""
        val prefsMemberId = getPrefs(_context, FN_MEMBER_GUID)
        prefsMemberId.getString(KN_MEMBER_GUID, "has not")?.let {
            guid = it
        }

        return guid
    }

    fun getMemberSeq(_context: Context): Int {
        val prefsMemberId = getPrefs(_context, FN_MEMBER_SEQ)
        return prefsMemberId.getInt(KN_MEMBER_SEQ, 0)
    }


    fun setAutoLoginVal(_context: Context, _isAutoLogin: Int) {
        getPrefs(_context, FN_AUTO_LOGIN).edit {
            putInt(KN_IS_AUTO_LOGIN, _isAutoLogin)
            apply()
        }
    }

    //TODO 저장할 값 변경
    fun setMemberGuid(_context: Context, _guid: String) {
        getPrefs(_context, FN_MEMBER_GUID).edit {
            putString(KN_MEMBER_GUID, _guid)
            apply()
        }
    }

    // Setting Member Seq
    fun setMemberSeq(_context: Context, _memSeq: Int) {
        getPrefs(_context, FN_MEMBER_SEQ).edit {
            putInt(KN_MEMBER_SEQ, _memSeq)
            apply()
        }
    }
}