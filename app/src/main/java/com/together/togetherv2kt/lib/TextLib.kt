package com.together.togetherv2kt.lib

import android.text.Editable
import android.widget.EditText
import java.util.*
import java.util.regex.Pattern

object TextLib {

    /**
     * 一桁の数字に0を付ける
     */
    fun makeTwoDigit(digit: Int): String {
        return String.format(Locale.getDefault(), "%02d", digit)
    }

    /**
     * 一桁の数字に0を付ける
     */
    fun makeTwoDigit(digit: String): String {
        return String.format(Locale.getDefault(), "%02d", digit)
    }

    /**
     * 数字からStringに変換
     */
    fun getStringFromNum(num: Int): String {
        return num.toString()
    }

    /**
     * パスワード有効性検査
     * 数字一つ、英文字一つ、特殊文字一つ以上を含めて6桁以上
     */
    fun isValidPassword(password: Editable): Boolean {
        val passwordPattern = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{6,}$"
        val pattern = Pattern.compile(passwordPattern)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    /**
     * パスワード有効性検査
     * 数字一つ、英文字一つ、特殊文字一つ以上を含めて6桁以上
     */
    fun isValidPassword(password: String): Boolean {
        val passwordPattern = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{6,}$"
        val pattern = Pattern.compile(passwordPattern)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    /**
     * Edit TextからString抽出
     */
    fun gatherTextFromEdt(_edt: EditText): String {
        return _edt.text.toString()
    }


    /**
     * TODO 나중에 쓰이게 고치자(edit text일 경우 포커스 이동)
     * パン詳細のEditTextを修正可能にする
     * EditText Focus移動
     * Soft Keyboard表示
     */
/*    private fun setEdtEnabled(status: Boolean) {
        edtContentPangDetail.isEnabled = status
        // Edit text 가 수정 가능한 상태이면 포커스 이동.
        if (status) {
            // Set edit text focus
            edtContentPangDetail.post {
                edtContentPangDetail.isFocusableInTouchMode = true
                edtContentPangDetail.requestFocus()
                val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                imm!!.showSoftInput(edtContentPangDetail, 0)
            }
        }
    }*/
}