package com.together.togetherv2kt.lib

import android.content.Context
import android.net.*
import retrofit2.Response

object NetLib {

    /**
     * Netに繋がっているかを確認
     * @param _context Context
     * @param _netCallback networkの状況による処理Callback
     * @return NetworkRequestとNetworkCallbackが登録されたConnectivityManager
     */
    fun checkNetState(_context: Context, _netCallback: ConnectivityManager.NetworkCallback): ConnectivityManager {
        val cm = _context.getSystemService(ConnectivityManager::class.java)
        val wifiNetworkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()

        cm.registerNetworkCallback(wifiNetworkRequest, _netCallback)
        return cm
    }

    fun <T> getResponseData(_res: Response<T>): T? {
        if (!_res.isSuccessful) {
            return null
        }

        return _res.body()
    }
}