package com.together.togetherv2kt.lib

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import java.util.*

object DialogLib {

    /**
     * @param _context Context
     * @param _arr text to setting dialog (0: msg, 1: button positive, 2: button negative)
     * @param func4PositiveBtn function for running when pushing positive button
     * @return Dialog
     */
    fun show2BtnDialog(_context: Context, _arr: IntArray, func4PositiveBtn: () -> Unit) {
        return MaterialDialog(_context)
            .message(_arr[0])
            .positiveButton(_arr[1]) { dialog ->
                func4PositiveBtn()
            }
            .negativeButton(_arr[2]) { dialog ->
                // Do something
            }.show()
    }

    fun makeDatePicker(
        _context: Context,
        dateListener: DatePickerDialog.OnDateSetListener
    ): DatePickerDialog {
        val calender = GregorianCalendar.getInstance()
        return DatePickerDialog(
            _context,
            dateListener,
            // set DatePickerDialog to point to today's date when it loads up
            calender.get(Calendar.YEAR),
            calender.get(Calendar.MONTH),
            calender.get(Calendar.DAY_OF_MONTH)
        )
    }

    fun makeTimePicker(
        _context: Context,
        timeListener: TimePickerDialog.OnTimeSetListener
    ): TimePickerDialog {
        val calender = GregorianCalendar.getInstance()
        return TimePickerDialog(
            _context,
            timeListener,
            calender.get(Calendar.HOUR),
            calender.get(Calendar.MINUTE),
            true
        )
    }
}