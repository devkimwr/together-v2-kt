package com.together.togetherv2kt.lib

import android.content.Context
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

object ImgLib {

    fun saveImage(_context: Context, myBitmap: Bitmap): File {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.PNG, 90, bytes)
        val dir = FileLib.getPublicAlbumStorageDir("pang")
        val f = File(dir, ((GregorianCalendar.getInstance().timeInMillis).toString() + ".png"))
        f.createNewFile()
        val fo = FileOutputStream(f)
        try {
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                _context,
                arrayOf(f.path),
                arrayOf("image/png"), null
            )
            fo.close()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return f
    }
}