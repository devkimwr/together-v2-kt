package com.together.togetherv2kt.lib

import android.os.Environment
import com.orhanobut.logger.Logger
import java.io.File


object FileLib {

    /* Checks if external storage is available for read and write */
    fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    /* Checks if external storage is available to at least read */
    private fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in
                setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
    }

    fun getPublicAlbumStorageDir(albumName: String): File? {
        if (!isExternalStorageReadable()) {
            Logger.d(" Not MEDIA_MOUNTED")
            return null
        }

        // Get the directory for the user's public pictures directory.
        val file = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            ), albumName
        )

        if (!file.mkdirs()) {
            Logger.d("Directory not created")
        }
        return file
    }
}