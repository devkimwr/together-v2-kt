package com.together.togetherv2kt.custom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.together.togetherv2kt.R

class RegistrationPlaceAdapter(
    context: Context,
    resource: Int,
    placeNameItems: List<String>
) : ArrayAdapter<String>(
    context,
    resource,
    placeNameItems
) {

    private val mPlaceNameItems by lazy {
        placeNameItems
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return customSpinnerView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return customSpinnerView(position, convertView, parent)
    }

    private fun customSpinnerView(position: Int, convertView: View?, parent: ViewGroup): View {
        //Getting the Layout Inflater Service from the system
        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //Inflating out custom spinner view
        val customView = layoutInflater.inflate(R.layout.v_spn_register_place, parent, false)
        //Declaring and initializing the widgets in custom layout
        val textView = customView.findViewById(R.id.tvRegisterPlace) as TextView
        //displaying the data
        textView.text = mPlaceNameItems[position]
        return customView
    }
}