package com.together.togetherv2kt.custom

import android.text.Editable
import android.text.TextWatcher

interface TxtWatcherCst : TextWatcher {
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {

    }
}